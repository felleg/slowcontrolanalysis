rm -f ./auto_html.o ./functions_bug_backup_dec2.o ./RootDictionary.o ./scatool.o SCATool 
g++  -I/usr/local/include -I/usr/local/include/root -I/sw/include -O2 -Wall -c scatool.cpp -o scatool.o
g++  -L/usr/local/lib/root -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic scatool.o -o sensorAnalysis

=== MENU ===
1: Slow Control Graphs
q: Quit SCATool

Selection: ||_____ a) Gas System Sensors Graphs
|______ b) HV Graphs

	Selection: ====================
Do you want to specify time range for every graph (y/n) (n = Plot all data): Enter starting time (YYYY/MM/DD hh:mm:ss): Accepted date: 2015/11/29 0:0:0
Enter ending time (set current time by leaving blank) (YYYY/MM/DD hh:mm:ss): Accepted date: 2015/11/30 0:0:0
Do you want to include this in a ROOT macro for drawing (y/n): File "slow-control-data.root" has been found in your directory. Do you want to append your graphs to this existing file? (n = crush the old file) (y/n): A new rootfile "slow-control-data.root" is created.
Fetching /raid/aither/SlowControlData/HV/ data...
Plotting HV extrapolations...

** Info **: No graph was saved to the rootfile (Tip: Are you certain your time range contained data?)
End of execution. Back to menu.

=== MENU ===
1: Slow Control Graphs
q: Quit SCATool

Selection: ============

Thanks for using the SCATool!
End of runSCATool.sh.
==================================
