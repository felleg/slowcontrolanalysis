#!/bin/bash
# This script is used to compile and execute the SCATool. It will also launch the root macro for the
# graphs you selected to analyze with the SCATool. It will also clean up after the end of execution,
# if the user wants it to.

# If you're confused, check the README.
# Felix Leger, 2015

make
if [ "$?" = 0 ];
then
	./sensorAnalysis
	n_drawn=$?
	rm sensorAnalysis
	if [ -e "root_commands.c" ] 
	then
		echo "Found \"root_commands.c\" in directory."
		if [ $n_drawn -gt 0 ]
		then
			echo "=== NOW ENTERING DRAWING PHASE ==="

			echo "Please select how you want your graphs drawn:" 
			echo "c: Open a ROOT canvas on your screen, which enables interactivity (ROOT will open in normal mode)"
			echo "s: Save graphs in the format of your choice (ROOT will open in batch mode)"
			echo "q: Quit without drawing anything"
			while :
			do
			read csq
			if [ "$csq" = "s" ] || [ "$csq" = "S" ]; then
				echo "Select save format for graphs"
				echo "(pdf png jpg gif eps svg tiff):"
				while :
				do 
				read save_format

				if [ "$save_format" = "eps" ]; then
					save_format=eps
					break
				elif [ "$save_format" = "pdf" ]; then
					save_format=pdf
					break
				elif [ "$save_format" = "svg" ]; then
					save_format=svg
					break
				elif [ "$save_format" = "gif" ]; then
					save_format=gif
					break
				elif [ "$save_format" = "png" ]; then
					save_format=png
					break
				elif [ "$save_format" = "jpg" ]; then
					save_format=jpg
					break
				elif [ "$save_format" = "tiff" ]; then
					save_format=tiff
					break
				else echo "Error: Invalid option. Try again. (pdf eps svg gif png jpg tiff)"
				fi
				done
				root -l -b -q 'root_commands.c("'$save_format'")'
				break
			elif [ "$csq" = "c" ] || [ "$csq" = "C" ]; then
				root -l 'root_commands.c("")'
				break
			elif [ "$csq" = "q" ] || [ "$csq" = "Q" ]; then
				break
			else echo "Error: Invalid option. Try again."
			fi
			done

		# If the user executed runSCATool.sh and no graphs were outputed
		else echo "It's probably an outdated root_commands.c file. You might as well delete it at this point."
		fi

		echo "Want to delete ROOT macro \"root_commands.c\"? (y/n): "
		while :
		do
		read csq
		if [ $csq = "y" ] || [ $csq = "Y" ]; then
		rm root_commands.c
		if [ "$?" = 0 ];
		then echo "Succesfully deleted \"root_commands.c\""
		else echo "Could not delete \"root_commands.c\""
		fi
			break
		elif [ $csq = "n" ] || [ $csq = "N" ]; then
		echo "Saved \"root_commands.c\" in:"; pwd; echo ""
			break
		else echo "Error: Invalid option. Try again. (y/n)"
		fi
		done
	fi

else
	echo "COMPILATION OF sensor-analysis.cpp FAILED"
fi
echo "Thanks for using the SCATool!"
echo "End of runSCATool.sh."
echo "=================================="
exit 0
