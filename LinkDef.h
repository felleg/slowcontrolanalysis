
// A list of classes Root does not understand by default and have to
// incorporated into a dictionary.
 
#include <vector>

#ifdef __MAKECINT__

#pragma extra_include "vector";

#pragma link C++ class vector<vector<unsigned int> >+;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<float> >+;
#pragma link C++ class vector<vector<double> >+;
//#pragma link C++ class pair<float,vector<float> >+;
//#pragma link C++ class pair<vector<float>,vector<float> >+;


#endif

