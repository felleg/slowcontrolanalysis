// To run this code properly, use html_update.sh

// This code is used by a cronjob to automatically generate the HTML pages where the SCATool Monitoring is provided
// (currently at www.hep.physics.mcgill.ca/~legerf/SCATool/)

// My directory (where the HTML pages are currently placed) is currently hard coded. This will maybe be modified 
// in the future to add flexibility, for example if my directory gets deleted.



#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <sys/stat.h>	// for stat, http://stackoverflow.com/a/12774387
#include <unistd.h>
#include <time.h>
using namespace std;

void sensor_monitoring_buttons(ofstream &); 
void fetch_graphs_write_names(ifstream &, ofstream &, string, string);
bool fexists(const char *);
void state_machine_picture(ofstream &);
void updated_or_not(ofstream &, string);
bool test_labview_running(string, string, ofstream &);
bool labview_update=-1;

int main()
{
	// List sensor graphs produced by the cron job in a text file
	system("ls /homes/babar/legerf/WWW/SCATool/sensors_graphs/month/ > list_of_sensors_graphs_month");
	system("ls /homes/babar/legerf/WWW/SCATool/sensors_graphs/week/ > list_of_sensors_graphs_week");
	system("ls /homes/babar/legerf/WWW/SCATool/sensors_graphs/day/ > list_of_sensors_graphs_day");
	system("ls /homes/babar/legerf/WWW/SCATool/HV_graphs/month/ > list_of_HV_month");
	system("ls /homes/babar/legerf/WWW/SCATool/HV_graphs/week/ > list_of_HV_week");
	system("ls /homes/babar/legerf/WWW/SCATool/HV_graphs/day/ > list_of_HV_day");
	ifstream input_month ("list_of_sensors_graphs_month");
	ifstream input_week ("list_of_sensors_graphs_week");
	ifstream input_day ("list_of_sensors_graphs_day");
	ifstream input_month_HV ("list_of_HV_month");
	ifstream input_week_HV ("list_of_HV_week");
	ifstream input_day_HV ("list_of_HV_day");

	string icon_code = "<link rel=\"icon\" type=\"image/png\" href=\"favicon-32x32.png\" sizes=\"32x32\" />\n<link rel=\"icon\" type=\"image/png\" href=\"favicon-16x16.png\" sizes=\"16x16\"/>\n";

	// Create output .html files
	ofstream output1("/homes/babar/legerf/WWW/SCATool/sensors_graphs_month.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output2("/homes/babar/legerf/WWW/SCATool/sensors_graphs_week.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output3("/homes/babar/legerf/WWW/SCATool/sensors_graphs_day.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output4("/homes/babar/legerf/WWW/SCATool/index.html", ios::out);
	ofstream output5("/homes/babar/legerf/WWW/SCATool/HV_graphs_month.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output6("/homes/babar/legerf/WWW/SCATool/HV_graphs_week.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output7("/homes/babar/legerf/WWW/SCATool/HV_graphs_day.html", ios::out); // for ios out, http://stackoverflow.com/q/12252430
	ofstream output8("/homes/babar/legerf/WWW/SCATool/state_machine_monitoring.html", ios::out);

	output1 << icon_code << "<p>\n<head><title>MONTHLY Sensors</title></head>\n";
	output2 << icon_code << "<p>\n<head><title>WEEKLY Sensors</title></head>\n";
	output3 << icon_code << "<p>\n<head><title>DAILY Sensors</title></head>\n";
	output4 << icon_code << "<p>\n<head><title>SCATool</title></head>\n<b><a href=\"about.html\">About this page</a><br><br>\n";
	output4 << "<button type=\"button\" style=\"background-color:yellow\" onclick=\"window.open('sensors_graphs_day.html');\">Start SCATool</button><br><button type=\"button\" style=\"background-color:aqua\" onclick=\"window.open('state_machine_monitoring.html');\">State Machine Monitoring</button><br><br>\n";

	labview_update = test_labview_running("/raid/aither/SlowControlData/StateMachine_Picture/", "StateMachine_Picture/", output4);	// If labview is not running, show error picture on index.html under buttons.

	output5 << icon_code << "<p>\n<head><title>MONTHLY HV</title></head>\n";
	output6 << icon_code << "<p>\n<head><title>WEEKLY HV</title></head>\n";
	output7 << icon_code << "<p>\n<head><title>DAILY HV</title></head>\n";
	output8 << icon_code << "<p>\n<head><title>State Machine Monitoring</title></head>\n";

	sensor_monitoring_buttons(output1);
	updated_or_not(output1, "");
	sensor_monitoring_buttons(output2);
	updated_or_not(output2, "");
	sensor_monitoring_buttons(output3);
	updated_or_not(output3, "");
	sensor_monitoring_buttons(output5);
	updated_or_not(output5, "");
	sensor_monitoring_buttons(output6);
	updated_or_not(output6, "");
	sensor_monitoring_buttons(output7);
	updated_or_not(output7, "");

	fetch_graphs_write_names(input_month, output1, "month", "sensors_graphs");
	fetch_graphs_write_names(input_week, output2, "week", "sensors_graphs");
	fetch_graphs_write_names(input_day, output3, "day", "sensors_graphs");
	fetch_graphs_write_names(input_month_HV, output5, "month", "HV_graphs");
	fetch_graphs_write_names(input_week_HV, output6, "week", "HV_graphs");
	fetch_graphs_write_names(input_day_HV, output7, "day", "HV_graphs");

	updated_or_not(output1, "<br>");
	sensor_monitoring_buttons(output1);
	updated_or_not(output2, "<br>");
	sensor_monitoring_buttons(output2);
	updated_or_not(output3, "<br>");
	sensor_monitoring_buttons(output3);
	updated_or_not(output5, "<br>");
	sensor_monitoring_buttons(output5);
	updated_or_not(output6, "<br>");
	sensor_monitoring_buttons(output6);
	updated_or_not(output7, "<br>");
	sensor_monitoring_buttons(output7);
	state_machine_picture(output8);	// We update the picture on the index

	output1 << "</p>\n";
	output2 << "</p>\n";
	output3 << "</p>\n";
	output4 << "</p>\n";
	output5 << "</p>\n";
	output6 << "</p>\n";
	output7 << "</p>\n";
	output8 << "</p>\n";

	system("rm list_of_sensors_graphs_month");
	system("rm list_of_sensors_graphs_week");
	system("rm list_of_sensors_graphs_day");
	system("rm list_of_HV_month");
	system("rm list_of_HV_week");
	system("rm list_of_HV_day");
	return 0;
}

void updated_or_not(ofstream & output, string br)
{
	if (labview_update) {output << br << "This page is updated every hour.<br>\n";}
	else {output << br << "<b><font color=\"red\">THERE SEEMS TO BE A PROBLEM WITH LABVIEW AT THE MOMENT. CLICK <a href=index.html>HERE</a> TO VIEW THE ERROR MESSAGE.</font></b><br>\n";}
}

void state_machine_picture(ofstream &output)
{
	output << "<font size=\"7\">State Machine Monitoring</font><br>(";
	if (labview_update) {output << "update every 15 mins";}
	else {output << "<b><font color=\"red\">THERE SEEMS TO BE A PROBLEM WITH LABVIEW AT THE MOMENT. CLICK <a href=index.html>HERE</a> TO VIEW THE ERROR MESSAGE.</font></b>";}
	output << ")<hr>\n";
	output << "<img src=\"StateMachine_Picture/state_machine_picture.png\"><br>\n";	// Show actual (or last valid) screenshot of LabVIEW State Machine

}

bool last_modified_exceeds(time_t period, const char* file)
{// This idea was taken from http://www.jb.man.ac.uk/~slowe/cpp/lastmod.html
	
	// Find moment of last modification of file
	time_t time_creation;
	struct tm* clock;				// create a time structure
	struct stat attrib;			// create a file attribute structure
	stat(file, &attrib);		// get the attributes of afile.txt
	clock = localtime(&(attrib.st_mtime));	// Get the last modified time and put it into the time structure
	time_creation = mktime(clock);

	// Find current time
	time_t time_now; 
	time(&time_now);

	if (time_now - time_creation > (60+period)*60)	// If there is a time length longer than "period" between time of creation and time now, the time is exceeded. (For some reason, there is a 1 hour difference between the windows clock and the unix system clock. The date of creation of the state machine picture is 1 hour too early last time I checked, which is why I add 60
	{
		return true;
	}
	return false;
}

bool test_labview_running(string path_raid, string path_WWW, ofstream &output)
{// If labview is not running (labview_not_running.png exists), add picture to the website.
	if (fexists((path_raid + "labview_not_running.png").c_str()))	// If no labview picture has been taken for the last 20 mins
	{
		output << "<img src=\"" << path_WWW << "labview_not_running.png\" title = \"LabVIEW is not running in lab 209.\">\n";	// Show labview_not_running picture on webpage
		cout << "LABVIEW IS NOT RUNNING. Added notice to SCATool webpage.\n";
		return 0;	// labview is NOT running
	}

	else if (last_modified_exceeds(20,(path_raid + "state_machine_picture.png").c_str()))	// This tests if the picture in the raid disk is older than 20 minutes (meaning the picture script on StateMachine computer failed to take a picture)
	{
		output << "<img src=\"" << path_WWW << "labview_maybe_not_running.png\" title = \"LabVIEW might not be running in lab 209.\">\n";
		cout << "LABVIEW MIGHT NOT BE RUNNING. Added notice to SCATool webpage.\n";
		return 0;
	}
	if (fexists((path_raid + "labview_not_recording.txt").c_str()))
	{
		output << "<img src=\"" << path_WWW << "labview_not_recording.png\" title = \"LabVIEW is not recording in lab 209.\">\n";	// Show labview_not_recording picture on webpage if labview_not_recording.txt file has been found in the raid disk
		cout << "LABVIEW IS NOT RECORDING DATA. Added notice to SCATool webpage.\n";
		return 0;
	}
	return 1;	// labview IS running properly
}


void sensor_monitoring_buttons(ofstream &output)
{
	output << "<hr>\n";
	output << "<button style = \"background-color:00FF00\" type=\"button\" onclick=\"location.href='sensors_graphs_day.html'\">DAILY</button>\n";
	output << "<button style = \"background-color:FFFF00\" type=\"button\" onclick=\"location.href='sensors_graphs_week.html'\">WEEKLY</button>\n";
	output << "<button style = \"background-color:FF0000\"  type=\"button\" onclick=\"location.href='sensors_graphs_month.html'\">MONTHLY</button>\n";
	output << "<b> --- SENSORS MONITORING</b><br>\n";
	output << "<button style = \"background-color:MediumAquaMarine\" type=\"button\" onclick=\"location.href='HV_graphs_day.html'\">DAILY</button>\n";
	output << "<button style = \"background-color:Orange\" type=\"button\" onclick=\"location.href='HV_graphs_week.html'\">WEEKLY</button>\n";
	output << "<button style = \"background-color:Fuchsia\"  type=\"button\" onclick=\"location.href='HV_graphs_month.html'\">MONTHLY</button>\n";
	output << "<b> --- HV MONITORING</b><br>\n";
	output << "<hr>\n";
}

void fetch_graphs_write_names(ifstream &input, ofstream &output, string timescale, string path)
{
	string line;
	while ( getline(input, line) )
	{
		if (line != "index.html")
		{
			output << "<img src=\"" << path << "/" << timescale << "/" << line << "\" title=\"" << line << "\">\n";
		}
	}
}

bool fexists(const char *filename)
{//Find if file exists, ripped from
//http://stackoverflow.com/a/12774387
  //struct stat buffer;   
  //return (stat (filename, &buffer) == 0); 
  ifstream ifile(filename);
  return ifile;
}
