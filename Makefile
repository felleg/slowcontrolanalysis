SHELL := /bin/bash
.SUFFIXES: .o .cpp .exe
APP_DIR = .
CPPFILES = $(shell ls . | grep ".cpp"| awk '{print("$(APP_DIR)/"$$0);}')
OBJECTS = $(CPPFILES:.cpp=.o)
EXECS = SCATool


ROOTINCDIR = $(shell root-config --incdir)
ROOTLIBS   = $(shell root-config --libs)

CPPFLAGS = -I/usr/local/include -I$(ROOTINCDIR) -I/sw/include
CFLAGS=-O2 -Wall

#all:	cleanall $(OBJECTS) $(EXECS)
all:	cleanall scatool.o $(EXECS)

SCATool: 
	g++  $(ROOTLIBS) scatool.o -o sensorAnalysis


#RootDictionary.cpp: LinkDef.h
rootdict: LinkDef.h
	rm -f RootDictionary.*
	@echo "**"
	@echo "** Creating Root dictionary"
	@echo "**"
	rootcint -f RootDictionary.cpp -c $<

#$(OBJECTS):
#	g++  $(CPPFLAGS) $(CFLAGS) -c $< -o $@

#$(RESULTS):
#	g++  $(ROOTLIBS) src/prepare-results.o -o prepare_results

#$(TEST):
#	g++  $(ROOTLIBS) src/test.o -o test


.cpp.o:
	g++  $(CPPFLAGS) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJECTS)

cleanall:
	rm -f $(OBJECTS) $(shell echo "$(EXECS:.exe=)" | awk '{for (i=1; i<=NF; i++){NSF=split($$i,j,"/"); printf("%s ", j[NSF]);};};')
