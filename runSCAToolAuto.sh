#!/bin/bash
# This script is used to start SCATool in automatic mode. Most probably used by an automatic cron job.
# (If you're confused, check the README file.)

# Felix Leger, 2015

alias brc='source ~/.bashrc'
source /usr/local/bin/thisroot.sh
make
if [ "$?" = 0 ];
then
	./sensorAnalysis -a "$@" # -a is a flag for automatic mode
	rm sensorAnalysis
	if [ -e "root_commands.c" ] 
	then
		echo "Found \"root_commands.c\" in directory."
		echo "=== NOW ENTERING DRAWING PHASE ==="
		save_format=png
		echo "All graphs saved as .$save_format" 

		echo "Opening ROOT in batch mode."
		root -l -b -q 'root_commands.c("'$save_format'")'
		rm slow-control-data.root	# not to overcrowd the disc
		rm root_commands.c		# not to overcrowd the disc

		if [ "$?" = 0 ];
		then echo "Succesfully deleted \"root_commands.c\""
		else echo "Could not delete \"root_commands.c\""
		fi
	else echo "No root_commands.c found."
	fi
	echo "Updating web page links"
	g++ -o auto_HTML auto_html.cpp && ./auto_HTML
	rm auto_HTML
else echo "Compilation of sensor-analysis-auto.cpp failed."
fi
echo "Thanks for using the SCATool!"
echo "End of runSCAToolAuto.sh."
echo "=================================="
exit 0
