// This header file is meant to be used by sensor-analysis.cpp
// You cannot run this code by itself. To use the tool, write in the terminal:
//	./runSCATool.sh
// Be sure to check the README to understand what is going on.

// Written by F�lix L�ger, July 2015 - Present (still in development)
// contact at: legerf@physics.mcgill.ca

#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include<limits>
#include<vector>
#include<stdlib.h>
#include<stdio.h>	//sscanf
#include<cmath>
#include <string>
#include <ctime>        // struct std::tm
#include <algorithm>	// find in vector<string> http://goo.gl/lhEK7q 

// ROOT stuff
#include<TFile.h>
#include "TGraph.h"
#include "TMath.h"
#include<TH2.h>
#include "TKey.h"
#include "TGraph2D.h"
#include "TDirectoryFile.h"
using namespace std;

// Global variables
bool automatic;	// Tells if we are running the tool in automatic mode
time_t start_time_global=0;	// Start time (global variable) (if specified by user)
time_t end_time_global=0;	// End time (global variable) (if specified by user)
bool setrang_or_not = 0; 	// Global variable to indicate if user at least once asked for a time range on their graph
bool only_24h=0;	// Tells if we request only to draw the last 24h (only for automatic mode)
string rootfile_name = "slow-control-data.root";	// The name of the ROOT file that will be produced by this code
string user_save_folder = "legerf";	// This is the username under which monitoring graphs will be saved.

// The list of functions
bool yes_no_choice();
void get_now_time(time_t &);	// modifies global variable end_time_global
void one_month_range(time_t &, time_t &);	// modifies global variable start_time_global
void time_of_execution(time_t);
void output_time_of_execution(ofstream &, bool);
void data_to_graph(vector<string> &, vector<string> &);
void correlation_graph(vector<string> &, vector<string> &, vector<string> &);
void analyze_HV(vector<pair<string, string> > &, vector<pair<string, string> > &, vector<pair<string, string> > &);
void f_is_time_ok(bool &, bool, struct tm &, time_t &, time_t, time_t, string, string);
float convert_cin_time(time_t &, bool);
bool find_graph_in_root(TFile *, string);
bool ask_draw(bool &);
void get_HV_color(string, int &);
bool ask_timerange(bool &);
bool ask_new_rootfile(string &);
bool rootfile_contains(string, string);
void end_execution(bool, bool, bool);
void set_HV_axis(TGraph *, string);
void list_menu(bool, bool, bool);
bool data_in_range(bool, string, time_t, time_t);
string replace_spaces_in_string(string, const char*);
string decide_pmt_or_tgc(string);
string generated_on();
vector<string> yaxis_title(string,bool,bool);
void comma_or_and(int &);
void extrapolate_HV_vmon_imon(TGraph*, TGraph*, vector<bool>);
void extrapolate_extremity_HV_graph(TGraph*, TGraph*, vector<bool>, time_t, bool);
void extrapolate(time_t , time_t , TGraph *, TGraph *, time_t , double );
void scan_HV_vmon_imon(TGraph*, TGraph*, vector<bool>, int);
void fill_correlation_graphs(string, string, TFile *, vector<string> &, vector<string> &, vector<string> &, bool);
void collect_data(int, int&, int , double* , double* , vector<double> & , double* , double* , vector<double> & , vector<double> &);
void output_commands_to_root(vector<string>, vector<string>, vector<string>, vector<string>, vector<pair<string, string> >, vector<pair<string, string> >, vector<pair<string, string> >, bool);
void output_min_max_hv_graph(ofstream &, string, string, string);
void output_min_max_hv_data_extrapol(ofstream &, string , time_t , time_t , string );
void output_declare_min_max(ofstream &, string , string , time_t , time_t );
void write_limits_commands(ofstream &, string, time_t, time_t, string, string, string, int, string);
void from_date_to_seconds(int, int, int, int, int, int, time_t &);
string from_seconds_to_date(time_t);
void create_list_directory(string);
template<typename T> bool new_vector_item(vector<T> &, T);
template <typename T> string to_string ( T Number );
bool filename_check(string);
time_t remove_hh_min_ss(time_t);
int set_n_box(int &);
bool fexists(const char *);
void set_xy_n_box(int &, int &);
int strpos(string, char, int);
void cout_oldest_newest_files(vector<string>);
void fill_HV_canvas(ofstream &, int, string, pair<string, string>, vector<pair<string, string> >, time_t, time_t, int, int);
void find_y_limits(ofstream &, string, time_t, time_t, string, int, string);
void find_y_limits_hv(ofstream &, string, string, time_t , time_t , int , int,  string);
void output_TGaxis_commands(ofstream &, string, string , string , int);
void output_scale_HV_imon(ofstream &, string , string );
void output_TGaxis_properties(ofstream &, string , string , string , int , string);


template <typename T> string to_string (T Number)	//Taken from http://www.cplusplus.com/articles/D9j2Nwbp/
{
   ostringstream ss;
   ss << Number;
   return ss.str();
}

template <typename T> bool new_vector_item(vector<T> &v, T item)
{//If we do not find "item" in the vector, it is added to the vector.
	if (std::find(v.begin(), v.end(), item) == v.end())
	{
		v.push_back(item);
		return 1;
	}
	return 0;
	
}

bool fexists(const char *filename)
{//Find if file exists, ripped from
 // http://www.cplusplus.com/forum/general/1796/
  ifstream ifile(filename);
  return ifile;
}

int strpos(string haystack, char needle, int nth)
{// Will return position of n-th occurence of a char in a string.
// Inspired by http://stackoverflow.com/a/18972477/5565172 
	string read;	// A string that will contain the read part of the haystack
	for (int i=1 ; i<nth+1 ; ++i)
	{
		std::size_t found = haystack.find(needle);
		read += haystack.substr(0,found+1); // the read part of the haystack is stocked in the read string
		haystack.erase(0, found+1);	// remove the read part of the haystack up to the i-th needle
		if (i == nth)
		{
			return read.size();
		}
	}
	return -1;
}

void end_execution(bool draw_or_not_sensors, bool draw_or_not_cor, bool draw_or_not_HV)
{// End of execution after a process has ended, and bring back the menu
	cout << "End of execution. Back to menu.\n";
	list_menu(draw_or_not_sensors,draw_or_not_cor, draw_or_not_HV);
}

void write_limits_commands(ofstream & output, string sensor_graph, time_t start_time, time_t end_time, string timescale, string color, string type_of_graph, int i, string canvas)
{// Will write commands in a ROOT macro (separate file). This section of the macro will modify the x-axis range of a graph, and then find the min-max y-axis values in that range, and then modify the y-axis range of the graph. Finally, save the graph.
	output << "\t\t" << sensor_graph << "->GetXaxis()->SetLimits(" << start_time << ", " << end_time << ");\n";	// Do NOT remove (SetRangeUser does not take effect (strangely) if SetLimits is not present beforehand)
	output << "\t\t" << sensor_graph << "->GetXaxis()->SetRangeUser(" << start_time << ", " << end_time << ");\n";

	find_y_limits(output, sensor_graph, start_time, end_time, color, i, canvas);

	output << "\t\tif (save_format.size()) " << canvas << i <<  "->SaveAs((\"/imports/WWW/people/" << user_save_folder << "/SCATool/" << type_of_graph << "/" << timescale << "/" << sensor_graph << "_" << timescale << ".\" + save_format).c_str());\n";
}

void output_declare_min_max(ofstream &output, string number, string graph_vmon, time_t start_time, time_t end_time)
{// This function is meant to be used 4 times in find_y_limits_hv (just a quick way to output commands to root without copy/pasting the same code over and over
	output << "\t\tdouble min" << number << " = 1111111;\n";
	output << "\t\tdouble max" << number << " = -1111111;\n";
	output << "\t\tfor (int i=0; i < " << graph_vmon << "->GetN(); ++i)\n";
	output << "\t\t{\n";
	output << "\t\t\tdouble x,y;\n";
	output << "\t\t\tx=y=0;\n";
	output << "\t\t\t" << graph_vmon << "->GetPoint(i,x,y);\n";
	output << "\t\t\tif (" << start_time << "<x && x<" << end_time << ")\n";
	output << "\t\t\t{\n";
	output << "\t\t\t\tif (y<min" << number << ") min" << number << "=y;\n\t\t\t\tif (max" << number << "<y) max" << number << "=y;\n";
	output << "\t\t\t}\n";
	output << "\t\t}\n";
}

void output_min_max_hv_data_extrapol(ofstream &output, string n, time_t start_time, time_t end_time, string graph)
{//This function outputs the commands necessary to find the maximum between the real data max and the extrapolation max for an HV graph.
	output_declare_min_max(output, n+"_1", graph, start_time, end_time);
	if (rootfile_contains(rootfile_name, (graph+"_extrapol").c_str()))
	{
		output_declare_min_max(output, n+"_2", graph+"_extrapol", start_time, end_time);
	}
	output << endl;
	output << "\t\t// Find the minimum and maximum of the vmon graph (between real data and the extrapolation)\n";
	output << "\t\tdouble min" << n << " = min" << n << "_1";
	if (rootfile_contains(rootfile_name, (graph+"_extrapol").c_str()))
	{
		output << " < min" << n << "_2 ? min" << n << "_1 : min" << n << "_2";
	}
	output << ";\n\t\tdouble max" << n << " = max" << n << "_1";
	if (rootfile_contains(rootfile_name, (graph+"_extrapol").c_str()))
	{
		 output << " > max" << n << "_2 ? max" << n << "_1 : max" << n << "_2";
	}
	output << ";\n\n";
}

void find_y_limits_hv(ofstream &output, string graph_vmon, string graph_imon, time_t start_time, time_t end_time, int color, int i, string canvas)
{// This function is used to find the max and min of the data inside a time range on a graph. If no data is in the graph, the text NO DATA AVAILABLE is shown.

	output_min_max_hv_data_extrapol(output, "1", start_time, end_time, graph_vmon);
	output_min_max_hv_data_extrapol(output, "2", start_time, end_time, graph_imon);


	
	output << "\t\tif ((min1 !=1111111 || max1 != -1111111) && (min2 !=1111111 || max2 !=-1111111))\n\t\t{\n";
	output << "\t\t\tdouble real_min = (min1 < min2 ? min1 : min2);\n";
	output << "\t\t\tdouble real_max = (max1 > max2 ? max1 : max2);\n";

	// If we use a legend, we want the graph to have a higher y-axis
	output << "\t\t\t" << graph_vmon << "->GetYaxis()->SetRangeUser(0,1.2*real_max);\n";	// deleteme(?) DO NOT deleteme this line. I am not sure if setting manually the minimum to be 0 is a good decision. This is currently a test.
	output << "\t\t\t" << canvas << i << "->Update();\n";
	output_TGaxis_commands(output, canvas, graph_vmon, graph_imon, color);
	output << "\t\t}\n";
	output << "\t\telse\n\t\t\t{\n";	
	output << "\t\t\t" << "TText *text = new TText();\n";
	output << "\t\t\t" << "text->SetNDC();\n";	
	output << "\t\t\t" << "text->SetTextFont(1);\n";
	output << "\t\t\t" << "text->SetTextColor(1);\n";
	output << "\t\t\t" << "text->SetTextSize(0.1);\n";
	output << "\t\t\t" << "text->SetTextAlign(22);\n";
	output << "\t\t\t" << "text->SetTextAngle(45);\n";
	output << "\t\t\t" << "text->DrawText(0.55,0.55,\"NO DATA AVAILABLE\");\n";
	output << "\t\t\t" << canvas << i << "->Update();\n";
	output << "\t\t\tTGaxis *axis_" << canvas << "_" << graph_imon << " = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(),gPad->GetUxmax(),gPad->GetUymax(),gPad->GetUymin(),gPad->GetUymax(),510,\"+L\");\n";	// There is one TGaxis per canvas (not taking any chances, here)
	output_TGaxis_properties(output, canvas, graph_vmon, graph_imon, color, "\t\t\t\t");
	output << "\t\t}\n";
}

void find_y_limits(ofstream &output, string sensor_graph, time_t start_time, time_t end_time, string color, int i, string canvas)
{// This function is used to find the max and min of the data inside a time range on a graph. If no data is in the graph, the text NO DATA AVAILABLE is shown.
	output << "\t\tdouble min = 1111111;\n";
	output << "\t\tdouble max = -1111111;\n";
	output << "\t\tfor (int i=0; i < " << sensor_graph << "->GetN(); ++i)\n";
	output << "\t\t{\n";
	output << "\t\t\tdouble x,y;\n";
	output << "\t\t\tx=y=0;\n";
	output << "\t\t\t" << sensor_graph << "->GetPoint(i,x,y);\n";
	output << "\t\t\tif (" << start_time << "<x && x<" << end_time << ")\n";
	output << "\t\t\t{\n";
	output << "\t\t\t\tif (y<min) min=y;\n\t\t\t\tif (max<y) max=y;\n";
	output << "\t\t\t}\n";
	output << "\t\t}\n";
	
	output << "\t\t" << "if (save_format.size())\n\t\t{\n";
	output << "\t\t\tif (min !=1111111 || max != -1111111)\n\t\t\t{\n";

	// If we use a legend, we want the graph to have a higher y-axis
	output << "\t\t\t\t" << sensor_graph << "->GetYaxis()->SetRangeUser(min,max);\n";
	output << "\t\t\t\t" << canvas << i << "->Update();\n";
	output << "\t\t\t}\n";
	output << "\t\t\telse\n\t\t\t{\n";	
	output << "\t\t\t\t" << "TText *text = new TText();\n";
	output << "\t\t\t\t" << "text->SetNDC();\n";	
	output << "\t\t\t\t" << "text->SetTextFont(1);\n";
	output << "\t\t\t\t" << "text->SetTextColor(" << color << ");\n";
	output << "\t\t\t\t" << "text->SetTextSize(0.1);\n";
	output << "\t\t\t\t" << "text->SetTextAlign(22);\n";
	output << "\t\t\t\t" << "text->SetTextAngle(45);\n";
	output << "\t\t\t\t" << "text->DrawText(0.55,0.55,\"NO DATA AVAILABLE\");\n";
	output << "\t\t\t}\n";
	output << "\t\t}\n";
}

void from_date_to_seconds(int yy, int mm, int dd, int hh, int min, int ss, time_t &converted_time)
{// This function takes numbers ripped from a date (yy/mm/dd hh:min:ss) and converts it to # of seconds since year 1900
	struct tm when;

	when.tm_year = yy-1900;	// year is difference between 1900 (http://goo.gl/HJWAMf) 
	when.tm_mon = mm-1;	// 0 is considered january
	when.tm_mday = dd;
	when.tm_hour = hh;
	when.tm_min = min;
	when.tm_sec = ss;
	when.tm_isdst = 0; // disable daylight saving time
	converted_time = mktime(&when);
}

string from_seconds_to_date(time_t converted_time)
{// This function takes a number of seconds and converts it to a date (yy/mm/dd hh:min:ss format)
	int yy=0, mm=0, dd=0, hh=0, min=0, ss=0;
	struct tm *when;

	when = localtime(&converted_time);
	yy = when->tm_year+1900;
	mm = when->tm_mon+1;
	dd = when->tm_mday;
	hh = when->tm_hour;
	min = when->tm_min;
	ss = when->tm_sec;

	// Idea stolen from http://www.cplusplus.com/forum/beginner/3405/
	stringstream sstream;
	sstream << yy << "/" << mm << "/" << dd << " " << hh << ":" << min << ":" << ss;
	string date = sstream.str();
 	return date;
}

bool rootfile_contains(string file_name, string word)
{// Check if a ROOTfile contains a certain object.
	if (fexists(rootfile_name.c_str()))	// Check first if the rootfile itself exists
	{
		TFile f(file_name.c_str());
		TIter next(f.GetListOfKeys());
		TKey *key;
		while ((key=(TKey*)next())) 
		{
			string name = key->GetName();
			if (name.find(word) != string::npos)	// If "word" is contained in the name of an item in the rootfile
			{	
				return 1;
			}
		}
	}
	return 0;	// If we did not find any item containing "word" in its name
}

void comma_or_and(int &elements)
{// This function is used to cout a pretty enumeration on the menu when we say what graphs are going to be plotted.

	// We check how many elements there are and then substract 1 because we outputed it in the screen
	if (elements >=3)
	{
		cout << ", ";
	}
	else if (elements == 2)
	{
		cout << " and ";
	}
	elements--;
	
}

void list_menu(bool draw_or_not_sensors, bool draw_or_not_cor, bool draw_or_not_HV)
{// Lists the main menu of the program.

	cout << "\n=== MENU ===\n1: Slow Control Graphs\n";

	if (rootfile_contains(rootfile_name, "Sensor_"))	// If user previously selected option 1a) (i.e. rootfile contains Sensor_<name> graphs), option 2 is unlocked.
	{
		cout << "2: Slow Control Sensors Correlation Graphs (found Sensor graphs in " << rootfile_name << " file)\n";
	}
	cout << "q: Quit SCATool";
	int elements = draw_or_not_sensors + draw_or_not_cor + draw_or_not_HV;	// Counts how many elements we have (to make a nice sentence in the menu)
	if (elements)
	{
		cout << " (will draw ROOT macro for ";
		if (draw_or_not_sensors)
			cout << "Sensor";
		comma_or_and(elements);
		if (draw_or_not_HV)
			cout << "HV";
		comma_or_and(elements);
		if (draw_or_not_cor)
			cout << "Correlation";
		cout << " graphs.)";
	}
	else if (fexists("root_commands.c")) cout << " (\"root_commands.c\" found in directory)";
	cout << "\n\nSelection: ";
}

string generated_on()
{// Gives time of execution (used mostly for timestamp on graphs)
	// Technique shamelessly ripped from http://www.cplusplus.com/reference/ctime/asctime/
	time_t rawtime;
	struct tm * timeinfo;
	string timer;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
	timer = asctime(timeinfo);
	timer = timer.substr(0,timer.size()-1);
	timer = "Generated on " + timer; 
	return timer;	
}

bool ask_new_rootfile(string &upd_or_recr)
{// Asks if user wants to create a new rootfile or simply update it
	bool new_rootfile=-1;

	if (fexists(rootfile_name.c_str()))
	{
		cout << "File \"" << rootfile_name << "\" has been found in your directory. Do you want to append your graphs to this existing file? (n = crush the old file) (y/n): ";

		if (yes_no_choice())
		{
			new_rootfile = 0;
		}
		else
		{
			new_rootfile = 1;
		}
	}
	if (new_rootfile)
	{
		cout << "A new rootfile \"" << rootfile_name << "\" is created.\n";
		upd_or_recr = "recreate";
		return 1;
	}
	else 
	{
		cout << "Graphs will be appended to existing rootfile \"" << rootfile_name << "\".\n";
		upd_or_recr = "update";
	}
	return 0;
}

bool yes_no_choice()
{// Prompts user to give a y/n answer to a question.
	bool decision=-1;
	string line;
	while (getline(cin, line))
	{
		char choice ='.';
		choice = line[0];
		if (choice == 'y' || choice == 'Y')
		{
			decision = true;
			break;
		}
		else if (choice == 'n' || choice == 'N') 
		{
			decision = false;
			break;
		}
		else cout << "Error: Invalid choice. Try again (y/n): ";
	}
	return decision;
}

bool ask_draw(bool &draw_or_not)
{// Checks if the user wants to draw the graphs
	cout << "Do you want to include this in a ROOT macro for drawing (y/n): ";
	if (yes_no_choice())
	{
		draw_or_not = 1;
	}
	else 
	{
		draw_or_not = 0;
	}
	return draw_or_not;
}

bool ask_timerange(bool &setrang_or_not)
{// Checks if user wants to select the range of their graphs


	// If user does option 1a) before 1b) (or vice versa), offer option to keep same range if they selected one.
	if (setrang_or_not)
	{
		cout << "You previously selected range " << from_seconds_to_date(start_time_global) << " ---- " << from_seconds_to_date(end_time_global) << ". (To change this range, restart the SCATool.)\n";

/* In previous versions, I offered the option of changing range once it was set (start_time_global and end_time_global). However, I realized this created some limitations: I had no way to be sure that for the HV graphs the value of end_time global would correspond to the value asked by the user (for example, when we want to use extrapolate_end_HV_graphs). Therefore, I removed this feature from the SCATool for now. I believe few users will actually notice a difference. At the worst, users will simply have to run the SCATool as many times as the number of ranges of time they wish to analyze.
On the positive side, now that start_time_global and end_time_global will be fixed once they are set, I will be able to do SetRangeUser in the output_commands_to_root() for the manual version of the SCATool: the graph will no longer be limited to the range of view of the data point. I will be able to set the range to actually what the user wanted. */

		//cout << "Want to keep using this range (y/n): ";	// These lines were set as a comment on January 13 2016
		//bool keep_range = 0;
		//keep_range = yes_no_choice();	// This could set keep_range value to 1
	}

	//if (keep_range == false)	// If keep_range has been modified by previous yes_no_choice, do not enter this block (would mean user wants to keep old time range)
	else
	{
		cout << "Do you want to specify time range for every graph (y/n) (n = Plot all data): ";
		if (yes_no_choice())
		{
			setrang_or_not = 1;
			while (1)
			{
				cout << "Enter starting time (YYYY/MM/DD hh:mm:ss): ";
				convert_cin_time(start_time_global, 0);
				cout << "Enter ending time (set current time by leaving blank) (YYYY/MM/DD hh:mm:ss): ";
				convert_cin_time(end_time_global, 1);
				if (start_time_global < end_time_global) break;
				else cout << "Error: Ending time must be after starting time. Please try again.\n";
			}
		}
	}
	
	return setrang_or_not;
}

int set_n_box(int &n_box)
{//Set number of boxes in a certain dimension (mostly for a 2D histogram)
	string line;
	getline(cin,line);
	if (line.size()) n_box = atoi(line.c_str());
	return n_box;
}

string replace_spaces_in_string(string filename, const char* replacement)
{//This function is used to replace spaces in a filename with "replacement" to be called in Unix later 
	string filename_copy = filename;
	size_t pos = filename_copy.find(" ",0);
	while (pos != string::npos)
	{
		//Since for example "\ " also contains a space, we must replace the space with another symbol during the while loop which searches for spaces
		filename_copy.replace(filename_copy.find(" "), 1, "�");
		pos = filename_copy.find(" ", pos+1);
	}
	pos = filename_copy.find("�",0);
	while (pos != string::npos)
	{
		filename_copy.replace(filename_copy.find("�"), 1, replacement);
		pos = filename_copy.find("�", pos+1);
	}
	return filename_copy;
}

void collect_data(int i, int &j, int n_pts_early, double *xaxis_early, double *yaxis_early, vector<double> &common_graph_early, double *xaxis_late, double *yaxis_late, vector<double> &common_graph_late, vector<double> &common_time)
{//For 2D histograms (correlation)
	// Find measurements which were taken at the same time
	while (j<n_pts_early && xaxis_early[j] <= xaxis_late[i])
	{//While there are still some early points left and early time must always be <= late time
		
		if (xaxis_early[j] == xaxis_late[i])
		{
			common_time.push_back(xaxis_early[j]);
			common_graph_early.push_back(yaxis_early[j]);
			common_graph_late.push_back(yaxis_late[i]);

			break;
		}
		j++;
	}
}

void create_list_directory(string path)
{// Will create a .sh file that will list directory and output 
 //it in a text file that will be read by this code
	ofstream script_file ("list_directory.sh");
	script_file << "ls " << path << "/ > data_files" << endl;	// replace_spaces(...) because must do "\ " in filenames in unix (when use system)
	script_file.close();
	system("chmod 755 list_directory.sh");
	system("./list_directory.sh");	
	system("if [ -e list_directory.sh ]\nthen rm list_directory.sh\nfi");	// The .sh file is removed, not needed anymore.
}		

float convert_cin_time(time_t &converted_time, bool enable_blank)
{// Will take a yy/mm/dd hh:min:ss string and transfer it to time in seconds since year 1900
// Here, converted_time can represent start_time_global or end_time_global
	int yy=0, mm=0, dd=0, hh=0, min=0, ss=0;
	string string_date;
	bool continue_loop=1;

	while (continue_loop && getline(cin, string_date))	// Infinite while loop until continue_loop=0
	{// This loop is prompting the user to give a plausible date

		if (string_date.empty() && enable_blank)
		{
			// Get current time into a tm structure (http://www.cplusplus.com/reference/ctime/tm/)
			time_t rawtime;
			struct tm * timeinfo;
			time (&rawtime);
			timeinfo = localtime (&rawtime);

			// Set yy, mm, etc. equal to relevant parts of the current time (year, month, etc.)
			yy=timeinfo->tm_year+1900; // add 1900 because year in tm structure is number of years since 1900
			mm=timeinfo->tm_mon+1;	// add 1 because january is 0 in tm structure but at this point in code, it is 1.
			dd=timeinfo->tm_mday;
			hh=timeinfo->tm_hour;
			min=timeinfo->tm_min;
			ss=timeinfo->tm_sec;

			continue_loop=0;
		}
		else
		{
			sscanf(string_date.c_str(), "%d/%d/%d %d:%d:%d", &yy, &mm, &dd, &hh, &min, &ss);
			time_t nowtime;	// This variable will contain the now time (in seconds)
			time (&nowtime);	// We fill the variable with the current time (in seconds)
			time_t test_time;	// This variable will contain the user specified time in seconds
			from_date_to_seconds(yy, mm, dd, hh, min, ss, test_time);	// We fill the variable with the user input

			if (2014<=yy && yy<2100 && 
			       0<mm && mm<13 && 
			       0<dd && dd<32 && 
			       0<=hh && hh<24 && 
			       0<=min && min<60 && 
			       0<=ss && ss<60)
			{
				if (test_time <= nowtime) // We make sure that the user specified time does not exceed the current time
				{
					continue_loop=0;	// If the date specified seems plausible, we exit the infinite while loop
				}
				else
				{
					cout << "Error: You cannot use a date past the current time. Try again: ";
				}
			}
			else
			{
				cout << "Error: The date specified cannot be used. Try again: ";
			}
		}
	}

	cout << "Accepted date: " << yy << "/" << mm << "/" << dd << " " << hh << ":" << min << ":" << ss << endl;

	from_date_to_seconds(yy, mm, dd, hh, min, ss, converted_time);	// This will modify converted_time

	return converted_time;	// Returns converted time (value around 600e6)
	
}

bool find_graph_in_root(TFile *file, string name)
{//Will search for graphs in the root file
	if (file->Get(("Sensor_"+name).c_str())) 
	{
		return 1;
	}
	else cout << "Error: Specified graph Sensor_" << name << " is not present in " << rootfile_name << ". Please try again." << endl;
	return 0;
}

void set_xy_n_box(int &xbox, int &ybox)
{// This functions sets the number of boxes for the 2D histograms of the correlation graphs
	cout << "\nSet number of boxes in xaxis and yaxis (default is 50x50): " << endl;

	cout << "# of boxes on x-axis (skip with \"Enter\"): ";
	set_n_box(xbox);
	cout << "# of boxes on y-axis (skip with \"Enter\"): ";
	set_n_box(ybox);
}

void fill_correlation_graphs(string first_name, string secnd_name, TFile *file, vector<string> &cor_graphs, vector<string> &cor_graphs_draw, vector<string> &sensor_graphs, bool manually)
{// This function fills the correlation graphs
	bool draw_or_not2=0;	// Draw or not for correlation graphs

	// Loads the selected graphs			
	TGraph *first_graph = (TGraph*)file->Get(("Sensor_"+first_name).c_str());
	TGraph *secnd_graph = (TGraph*)file->Get(("Sensor_"+secnd_name).c_str());	

	// Variables used to set range (if user decides to do so)
	double min_value1=1111111;	// Min value for graphs range (will go down over data)
	double max_value1=-1111111;	// Max value for graphs range (will go up over data)
	double min_value2=1111111;	// Min value for graphs range (will go down over data)
	double max_value2=-1111111;	// Max value for graphs range (will go up over data)

	cout << "Correlation graphs for Sensor_" << first_name << " and Sensor_" << secnd_name << " will be appended to " << rootfile_name << ".\n";
	if (manually)
	{
		ask_draw(draw_or_not2);	// Note: we do not ask to set range, because it uses graphs from sensor-analysis, where we already asked to set range
	}
	else	// If we are in automatic mode...
	{
		draw_or_not2=1;	// ... automatically draw the cor graphs.
	}

	int n_pts1 = first_graph->GetN();	//Get number of points graph 1
	int n_pts2 = secnd_graph->GetN();	//Get number of points graph 2
	int n_pts_max = (n_pts1 > n_pts2 ? n_pts1 : n_pts2);	// see http://www.cplusplus.com/forum/beginner/106519/

	double *xaxis1 = first_graph->GetX();
	double *yaxis1 = first_graph->GetY();
	double *xaxis2 = secnd_graph->GetX(); 
	double *yaxis2 = secnd_graph->GetY();

	vector<double> common_time;
	vector<double> common_graph1;
	vector<double> common_graph2;

	cout << "\nNow retrieving data..." << endl;
	int j=0;
	for (int i=0; i<n_pts_max; ++i)
	{
		// Must find which sensor started taking data sooner, because otherwise causes while loop to never end 
		switch(xaxis1[i] < xaxis2[i])
		{
			case 0:
				collect_data(	i,
						j,
						n_pts2,
						xaxis2,
						yaxis2,
						common_graph2,
						xaxis1,
						yaxis1,
						common_graph1,
						common_time);
				break;
			case 1:
				collect_data(	i,
						j,
						n_pts1,
						xaxis1,
						yaxis1,
						common_graph1,
						xaxis2,
						yaxis2,
						common_graph2,
						common_time);
				break;
		}
	}

	// If there were some measurements at common time... (there are correlations to be made)
	if (common_time.size())
	{
		// Set the number of boxes in the correlation graph
		int xbox = 50;
		int ybox = 50;
		if (manually)
		{
			set_xy_n_box(xbox,ybox);
		}

		// Add the selected cor graphs to the list of selected cor graphs
		// Take name of first sensor graph
		cor_graphs.push_back(replace_spaces_in_string("Sensor_"+first_name, "_"));
		// Take name of second sensor graph
		cor_graphs.push_back(replace_spaces_in_string("Sensor_"+secnd_name, "_"));
		//Take name of the first cor graph
		cor_graphs.push_back(replace_spaces_in_string("Cor_Hist_"+first_name+"_"+secnd_name, "_"));
		//Take name of the second cor graph
		cor_graphs.push_back(replace_spaces_in_string("Cor_Graph2D_"+first_name+"_"+secnd_name, "_"));	

		// If user decides to draw the histograms, we keep the names of the things we must draw in a vector
		if (draw_or_not2)
		{	

			// This is in case user does option 2) without option 1) in the menu (already has rootfile). Keeps memory of selected sensors
			if (manually)
			{
				new_vector_item(sensor_graphs, replace_spaces_in_string("Sensor_"+first_name, "_"));
				new_vector_item(sensor_graphs, replace_spaces_in_string("Sensor_"+secnd_name, "_"));
			}

			// Add the cor graphs that the user wants drawn to the drawing list
			// Take name of first sensor graph
			cor_graphs_draw.push_back(replace_spaces_in_string("Sensor_"+first_name, "_"));
			// Take name of second sensor graph
			cor_graphs_draw.push_back(replace_spaces_in_string("Sensor_"+secnd_name, "_"));
			//Take name of the first cor graph
			cor_graphs_draw.push_back(replace_spaces_in_string("Cor_Hist_"+first_name+"_"+secnd_name, "_"));
			//Take name of the second cor graph
			cor_graphs_draw.push_back(replace_spaces_in_string("Cor_Graph2D_"+first_name+"_"+secnd_name, "_"));	
		}

		
		TH2D *cor_hist = new TH2D (("Cor_Hist_"+first_name+"_"+secnd_name).c_str(), ("Correlation Histogram between " + first_name + " and " + secnd_name).c_str(), xbox, min_value1, max_value1, ybox, min_value2, max_value2);
		for (size_t i=0; i<common_graph1.size(); ++i)
		{
			cor_hist->Fill(common_graph1.at(i), common_graph2.at(i));
		} 
		cor_hist->GetXaxis()->SetTitle(first_graph->GetYaxis()->GetTitle());
		cor_hist->GetYaxis()->SetTitle(secnd_graph->GetYaxis()->GetTitle());

		// Correlation histogram is appended to the rootfile
		cor_hist->Write();


		// Save 3D correlation graph to ROOT file
		TGraph2D *cor_graph2d = new TGraph2D(common_time.size(), &(common_time[0]), &(common_graph1[0]), &(common_graph2[0]));	// The 2D graph summarizes the 3 others
		cor_graph2d->SetName(replace_spaces_in_string(("Cor_Graph2D_"+first_name+"_"+secnd_name), "_").c_str());
		cor_graph2d->SetTitle(("Correlation Graph between " + first_name + ", " + secnd_name + " and time").c_str());
		cor_graph2d->GetXaxis()->SetTimeDisplay(1);	//Display time axis
		cor_graph2d->GetXaxis()->SetTitle("Time #splitline{(YY/MM/DD)}{(hh:mm:ss)}");
		cor_graph2d->GetXaxis()->SetTimeOffset(0, "UTC");
		cor_graph2d->GetXaxis()->SetTimeFormat("#splitline{\%Y/\%m/\%d}{\%H:\%M:\%S}");
		cor_graph2d->GetYaxis()->SetTitle(first_graph->GetYaxis()->GetTitle());
		cor_graph2d->GetZaxis()->SetTitle(secnd_graph->GetYaxis()->GetTitle());

		// Correlation histogram is appended to the rootfile
		cor_graph2d->Write();

		cout << "Successfully appended correlation graphs of Sensor_" << first_name << " and Sensor_" << secnd_name << " to \"" << rootfile_name << "\"." << endl;
	}
	else cout << "Error: Sensor_" << first_name << " and Sensor_" << secnd_name << " have never taken measurements at same time. No correlation graph can be made." << endl;
}


void cout_oldest_newest_files(vector<string> data_files_vector)
{// This function outputs on the screen the newest and oldest file read for a given sensor (or HV). This is because sometimes the user can ask for a date which does not contain data in the system (the data file for that date would not exist). This function would help the user see if that is the case.
	if (data_files_vector.size())
	{
		cout << "\tOldest file in specified range: " << data_files_vector.at(0) << endl;
		cout << "\tNewest file in specified range: " << data_files_vector.at(data_files_vector.size()-1) << endl;
	}
}

bool filename_check(string data_file_name)
{	// Checks if the data file found in the sensor's folder should be opened (0==NO, 1==YES)
	if (data_file_name.find("Trans_") != string::npos)
	{
		cout << "\tIgnored " << data_file_name << " (contained \"Trans_\")." << endl;	
		return 0;
	}
	else if (data_file_name.find("Conflict") != string::npos)
	{
		cout << "\tIgnored " << data_file_name << " (contained \"Conflict\")." << endl;
		return 0;
	}
	else if (data_file_name.find("desktop.ini") != string::npos)
	{
		cout << "\tIgnored " << data_file_name << " (contained \"desktop.ini\")." << endl;
		return 0;
	}
	else if (data_file_name.find("(") != string::npos || data_file_name.find("(") != string::npos)
	{
		cout << "\tIgnored " << data_file_name << " (contained parantheses, indicating clone file)." << endl;
		return 0;
	}
	else	return 1;
}

string decide_pmt_or_tgc(string board_channel)
{//This function determines (by reading config.txt) if a board is a PMT or a TGC.
	string PMT_or_TGC = "???";	// If board number is not defined in config, we output ??? in the HV graph title
	string board = board_channel.substr(0,strpos(board_channel, '_', 1)-1);
	ifstream myfile ("config.txt");
	string line;
	
	if (myfile.is_open())
	{
		while ( getline(myfile, line) )
		{
			//If we find in config.txt something like "Board #"
			if (line.find(("Board " + board + ":").c_str()) != string::npos)
			{
				size_t pos = line.find(":");
				PMT_or_TGC = line.substr(pos);
				PMT_or_TGC.erase(0,2);	// Remove ": "
				break;
			}
		}
	}
	return PMT_or_TGC;
}

void f_is_time_ok(bool &is_time_ok, bool setrang_or_not, struct tm &when, time_t &converted_time, time_t start_time_global, time_t end_time_global, string file, string date_time)
{//Get the time of measurement (http://goo.gl/G2h83p)

	int yy=0, mm=0, dd=0, hh=0, min=0, ss=0;
	sscanf(date_time.c_str(), "%d/%d/%d %d:%d:%d", &yy, &mm, &dd, &hh, &min, &ss);
	from_date_to_seconds(yy, mm, dd, hh, min, ss, converted_time);
	
	// Here we verify if the exact time of measurement falls inside the specified time interval
	switch (setrang_or_not)
	{
		case 0:
			if (2014<=yy && yy<2100 
			    && 1<=mm 
			    && mm<=12 
			    && 1<=dd 
			    && dd<=31 
			    && 0<=hh 
			    && hh<24 
			    && 0<=min 
			    && min<60 && 0<=ss 
			    && ss<60)	//See if timestamp is plausible
			{
				is_time_ok=true;
			}
			else	// If timestamp not plausible, will not include measurement in analysis
			{
					cout << "\t" << file << " contains an impossible timestamp (\"";
					if (!(yy+mm+dd+hh+min+ss))
					{
						cout << "empty line?";
					}
					else
					{
						cout << yy << "/" << mm << "/" << dd << " " << hh << ":" << min << ":" << ss;  
					}
					cout << "\") which was ignored. Please fix." << endl; 
			}
			break;

		case 1:
			if (start_time_global <= converted_time && converted_time <= end_time_global)	// If time of measurement if between specified interval, we save it!
			{
				is_time_ok=true;
			}
			break;
		default:
			is_time_ok=false;
			break;

	}
}							

vector<string> yaxis_title(string sensor_name, bool erase, bool cout_not_found)
{//This function is used to give an appropriate name (and units) to the yaxis of a graph
 // It will return a vector containing all the info for a specific sensor.
 // The info is from the config.txt file.
	if (erase)	// We might use this function for a sensor without the path name
	{
		sensor_name.erase(0,29);	// Will remove the path name from the sensor name so that it looks good when we use it to name graphs
	}
	
	string line;
	ifstream myfile("config.txt");
	vector<string> returned_vector;

	if (myfile.is_open())
	{
		while ( getline(myfile, line) )
		{
			if (line.find(sensor_name.c_str()) != string::npos)
			{
				returned_vector.push_back(line);	// Sensor name
				getline(myfile, line);
				returned_vector.push_back(line);	// Sensor name in y-axis form (w/ units)
				getline(myfile, line, ',');
				returned_vector.push_back(line);	// Graph default ymin value
				getline(myfile, line);
				returned_vector.push_back(line);	// Graph default ymax value
				getline(myfile, line);
				returned_vector.push_back(line);	// Marker color in graph
				
				return returned_vector;
			}
		}
	}
	else cout << "Could not open config.txt\n";

	if (cout_not_found) cout << "\tInfo: Sensor " << sensor_name << " not found in config.txt. Marked as Unknown Sensor\n";	// Since this function is used at many places in the code, we do not always want this line to get repeated if a sensor is not found.
	returned_vector.push_back(sensor_name.c_str());
	returned_vector.push_back(("Unknown " + sensor_name).c_str());
	returned_vector.push_back("-99999");
	returned_vector.push_back("99999");
	returned_vector.push_back("46");	// Marker color that is now used for other graphs
	return returned_vector;
}


bool data_in_range(bool setrang_or_not, string file_name, time_t start_time_global, time_t end_time_global)
{// If user specified they wanted a fix range, we will open open files from within that range.
 // 0 == NO OPEN, 1 == OPEN
	if (setrang_or_not)	// If user requested range, we check
	{
		// A typical filename would be Amb_Humidity_2015-03-27.txt
		// We find the '-' character in filename, because it indicates where the date is.
		int yy=0, mm=0, dd=0, hh=0, min=0, ss=0;
		struct tm when;
		int pos=0;
		pos=file_name.find('-')-4;	// Substract 4 because we want to start reading the string from the year number.
		string date_in_file_name = file_name.substr(pos,10);
//		file_name.erase(0,pos);	// Remove everything before the date in the filename
//		file_name.erase(10,4);	// Remove the .txt extension

		// Extract year, month and data from the data file name
		sscanf(date_in_file_name.c_str(), "%d-%d-%d", &yy, &mm, &dd);	

		when.tm_year = yy-1900;	// year is difference between 1900 (http://goo.gl/HJWAMf) 
		when.tm_mon = mm-1;	// 0 is considered january
		when.tm_mday = dd;
		when.tm_hour = hh;	// hour is not in file name, so is set to 0
		when.tm_min = min;	// min is not in file name, so is set to 0
		when.tm_sec = ss;	// sec is not in file name, so is set to 0
		time_t converted_time;
		converted_time = mktime(&when);

		// since the converted_time has hh=0, min=0, ss=0, we must remove it also from start_time and end_time using the remove_hh_min_ss function

		if (remove_hh_min_ss(start_time_global) <= converted_time && converted_time <= remove_hh_min_ss(end_time_global))
		{
			return 1;
		}
		else return 0;
	}
	else return 1;	// If user did not request range, the data is always in range.
}

time_t remove_hh_min_ss(time_t some_time)
{//This function takes a time (time_t) and sets the hour, min and seconds to zero. 
	struct tm * when;
	when = localtime(&some_time);
	when->tm_hour=0;
	when->tm_min=0;
	when->tm_sec=0;
	some_time = mktime(when);
	return some_time;
}

void output_min_max_hv_graph(ofstream &output, string hv_graph, string min_max, string Min_Max)
{//This function exists because I have to find the min and the max of the vmon and imon graphs, and especially find the max between the real data and the extrapolation. It is extremely long to write, so I simply put it as a function that I will re-use 4 times later.
	output << "\t\tdouble " << min_max << "_real_data = TMath::" << Min_Max << "Element(" << hv_graph << "->GetN()," << hv_graph << "->GetY());\n";
	if (rootfile_contains(rootfile_name, (hv_graph+"_extrapol").c_str()))
	{
		output << "\t\tdouble " << min_max << "_extrapol_data = TMath::" << Min_Max << "Element(" << hv_graph << "->GetN()," << hv_graph << "->GetY());\n";
	}
	output << "\t\tdouble " << min_max << "_" << hv_graph << " = " << min_max << "_real_data";
	if (rootfile_contains(rootfile_name, (hv_graph+"_extrapol").c_str()))
	{
		output << " < " << min_max << "_extrapol_data ? " << min_max << "_real_data : " << min_max << "_extrapol_data"; 
	}
	output << ";\n\n";
}

void output_scale_HV_imon(ofstream &output, string hv_graph_vmon, string hv_graph_imon)
{//This function is used to output commands in the root_commands.c file that will find the max element of an HV graph and then scale the imon part of that graph. NOTE: after using this function, the graph will be modified (within the same execution of root_commands.c). Using this function twice will end up in scaling the graph twice.
	output << "\t\t// Find the minimum and maximum of vmon and imon in HV graph\n";
	output_min_max_hv_graph(output, hv_graph_vmon, "min", "Min");	// Finds the vmon min (between the real data and the extrapolation)
	output_min_max_hv_graph(output, hv_graph_imon, "min", "Min");	// similar...
	output_min_max_hv_graph(output, hv_graph_vmon, "max", "Max");	// etc.
	output_min_max_hv_graph(output, hv_graph_imon, "max", "Max");

	output << "\t\tdouble ratio_max_" << hv_graph_vmon << "_VS_max_" << hv_graph_imon << " = 1;\n";
	output << "\t\tif (min_" << hv_graph_imon << " < max_" << hv_graph_imon << ")\n\t\t{\n\t\t\t double ratio_max_" << hv_graph_vmon << "_VS_max_" << hv_graph_imon << " = 1.2*max_" << hv_graph_vmon << "/max_" << hv_graph_imon << " - 1;\n\t\t}\n";
	output << "\t\t// We want to scale the data points of the imon so that they fill the whole pad\n";
	output << "\t\tfor (int i=0; i < " << hv_graph_imon << "->GetN(); ++i)\n\t\t{\n\t\t\t//We do not mistakenly divide by 0:\n\t\t\tdouble x, y;\n\t\t\t" << hv_graph_imon << "->GetPoint(i,x,y);\n\t\t\t" << hv_graph_imon << "->SetPoint(i,x,y * ratio_max_" << hv_graph_vmon << "_VS_max_" << hv_graph_imon << ");\n\t\t}\n";
	if (rootfile_contains(rootfile_name, (hv_graph_imon + "_extrapol").c_str()))
	{
		output << "\t\tfor (int i=0; i < " << hv_graph_imon << "_extrapol->GetN(); ++i)\n\t\t{\n\t\t\tdouble x, y;\n\t\t\t" << hv_graph_imon << "_extrapol->GetPoint(i,x,y);\n\t\t\t" << hv_graph_imon << "_extrapol->SetPoint(i,x,y * ratio_max_" << hv_graph_vmon << "_VS_max_" << hv_graph_imon << ");\n\t\t}\n";
	}
}

void fill_HV_canvas(ofstream &output, int i, string timescale, pair<string, string> HV_graphs_draw, vector<pair<string, string> > HV_graphs_extrapol, time_t start_time, time_t end_time, int color, int n_canvas)
{// This function is used to output commands to ROOT, in particular to create HV graphs. A canvas is created for each (month, week, day). This method (3 canvases) is used because we were having issues with TGaxis if we used a single canvas that we modified to plot the 3 distinct graphs. 
// With the if(automatic) section, we set the time range. This is only for the automatic mode where we draw month, week and day graphs. This could be added for manual mode, but is not yet implemented.

	string canvas_name = "";	// Canvas name can be d, dd or ddd based on if it is canvas for month, week or day respectively
	for (int n=0; n < n_canvas; ++n)
	{
		canvas_name += "d";
	}

	output << "\t\tTCanvas *" << canvas_name << i << " = new TCanvas(\"" << HV_graphs_draw.first << "_imon" << timescale << "\", \"" << HV_graphs_draw.first << "_imon_" << timescale << "\");\n";	
	output << "\t\t" << canvas_name << i << "->SetGrid();\n";
	output << "\t\t" << canvas_name << i << "->SetRightMargin(0.11);\n";
	output << "\t\t" << canvas_name << i << "->cd();\n";

	if (!automatic)
	{// Set range for manual mode
	}
	output << "\t\t" << HV_graphs_draw.first << "->Draw(\"ap\");\n";
	output << "\t\t" << HV_graphs_draw.second << "->Draw(\"psame\");\n";

	for (size_t j=0; j < HV_graphs_extrapol.size(); ++j)
	{
		// If the name of the HV graph and the extrapolation graph is the same, we plot the extrapolation graph on the HV graph
		if ( !HV_graphs_extrapol.at(j).first.substr(0,HV_graphs_draw.first.size()).compare(HV_graphs_draw.first) &&
		     !HV_graphs_extrapol.at(j).second.substr(0,HV_graphs_draw.second.size()).compare(HV_graphs_draw.second))
		{
			string line = HV_graphs_extrapol.at(j).first.substr(0,3);
			int color2=0;
			get_HV_color(line, color2);
			output << "\t\t" << HV_graphs_extrapol.at(j).first << "->SetMarkerSize(0.5);\n";
			output << "\t\t" << HV_graphs_extrapol.at(j).first << "->SetMarkerStyle(4);\n";
			output << "\t\t" << HV_graphs_extrapol.at(j).second << "->SetMarkerColor(" << color2 << ");\n";
			output << "\t\t" << HV_graphs_extrapol.at(j).second << "->SetMarkerSize(0.5);\n";
			output << "\t\t" << HV_graphs_extrapol.at(j).second << "->SetMarkerStyle(4);\n";

			output << "\t\t" << HV_graphs_extrapol.at(j).first << "->Draw(\"psame\");\n";
			output << "\t\t" << HV_graphs_extrapol.at(j).second << "->Draw(\"psame\");\n";
		}
	}

	output_time_of_execution(output, 0);
	output << "\t\tleg_" << HV_graphs_draw.first << "->Draw(\"SAME\");\n";

	// This sets correct range, based on interval we're looking at (month, week, etc)
	if (automatic)
	{
		output << "\t\t" << HV_graphs_draw.first << "->GetXaxis()->SetLimits(" << start_time << "," << end_time << ");\n";	// Do NOT remove (SetRangeUser does not take effect (strangely) if SetLimits is not present beforehand)	
		output << "\t\t" << HV_graphs_draw.first << "->GetXaxis()->SetRangeUser(" << start_time << "," << end_time << ");\n";
		find_y_limits_hv(output, HV_graphs_draw.first, HV_graphs_draw.second, start_time, end_time, color, i, canvas_name);
	}
	else	// Same idea, but for manual use of the scatool
	{
		output << "\t\t//Finds the real maximum in the graph (could be vmon or imon), so that no data point gets ignored\n";
		output << "\t\tdouble new_min_" << HV_graphs_draw.second << " = TMath::MinElement(" << HV_graphs_draw.second << "->GetN()," << HV_graphs_draw.second << "->GetY());\t\n";
		output << "\t\tdouble new_max_" << HV_graphs_draw.second << " = TMath::MaxElement(" << HV_graphs_draw.second << "->GetN()," << HV_graphs_draw.second << "->GetY());\n";
		output << "\t\tdouble real_min_" << HV_graphs_draw.first << "_imon = (min_" << HV_graphs_draw.first << " < new_min_" << HV_graphs_draw.second << " ? min_" << HV_graphs_draw.first << " : new_min_" << HV_graphs_draw.second << ");\n";
		output << "\t\tdouble real_max_" << HV_graphs_draw.first << "_imon = (max_" << HV_graphs_draw.first << " > new_max_" << HV_graphs_draw.second << " ? max_" << HV_graphs_draw.first << " : new_max_" << HV_graphs_draw.second << ");\n";
		output << "\t\t" << HV_graphs_draw.first << "->GetYaxis()->SetRangeUser(real_min_" << HV_graphs_draw.first << "_imon, 1.2*real_max_" << HV_graphs_draw.first << "_imon);\n";
		output << "\t\t// Set time range of the HV graph\n";
		output << "\t\t" << HV_graphs_draw.first << "->GetXaxis()->SetLimits(" << start_time_global << "," << end_time_global << ");\n";	// Do NOT remove (SetRangeUser does not take effect (strangely) if SetLimits is not present beforehand)
		output << "\t\t" << HV_graphs_draw.first << "->GetXaxis()->SetRangeUser(" << start_time_global << "," << end_time_global << ");\n";
		output << "\t\t//Must update the canvas so that we can access its size with gPad\n";
		output << "\t\t" << canvas_name << i << "->Update();\n";
		output_TGaxis_commands(output, canvas_name, HV_graphs_draw.first, HV_graphs_draw.second, color);
	}

	// Save the HV graph
	output << "\t\t" << "if (save_format.size()) " << canvas_name << i <<  "->SaveAs((\"";
	if (automatic)
	{
		output << "/imports/WWW/people/" << user_save_folder << "/SCATool/HV_graphs/" << timescale << "/";
	}
	output << HV_graphs_draw.first << "_imon"; 
	if (automatic) 
	{
		output << "_" << timescale; 
	}
	output << ".\" + save_format).c_str());\n";
}

void output_TGaxis_properties(ofstream &output, string canvas_name, string hv_graph_vmon, string hv_graph_imon, int color, string tabulation)
{//This function is used to output the properties of a TGaxis in root_commands.c
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->SetLineColor(" << color << ");\n";
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->SetLabelColor(" << color << ");\n";
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->SetTextColor(" << color << ");\n";
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->SetTitle(\"IMon (#muA)\");\n";
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->SetTitleOffset(1.4);\n";
	output << tabulation << "axis_" << canvas_name << "_" << hv_graph_imon << "->Draw();\n";
}

void output_TGaxis_commands(ofstream &output, string canvas_name, string hv_graph_vmon, string hv_graph_imon, int color)
{//This function outputs the commands to draw the TGaxis for the HV graphs. It is outputed in the root_commands.c file
	string tabulation = "\t\t";	
	if (automatic)
	{
		tabulation+="\t";  // This is just for visual purposes in the root_commands.c file. In the automatic mode, because of the if, a new tab is added.
		output << tabulation << "//We ensure that the imon min and max are not equal, otherwise the TGaxis will not be able to be drawn\n" << tabulation << "if (min2 == max2)\n" << tabulation << "{\n" << tabulation << "\tmax2++;\n" << tabulation << "}\n";
	}
	output << tabulation << "TGaxis *axis_" << canvas_name << "_" << hv_graph_imon << " = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(),gPad->GetUxmax(),gPad->GetUymax(),gPad->GetUymin(),1.2*real_max";

	if (!automatic)
	{	//This is because real_max has a different symbol in the automatic and manual versions
		output << "_" << hv_graph_vmon << "_imon";
	}
	output << "/ratio_max_" << hv_graph_vmon << "_VS_max_" << hv_graph_imon;	
	output << ",510,\"+L\");\n";	// There is one TGaxis per canvas (not taking any chances, here)
	output_TGaxis_properties(output, canvas_name, hv_graph_vmon, hv_graph_imon, color, tabulation);
}


void get_HV_color(string PMT_or_TGC, int &color)
{// This function returns the ROOT Color specified in the config.txt file for either a PMT or a TGC (specified in "PMT_or_TGC")

	string line;
	ifstream myfile("config.txt");
	if (myfile.is_open())
	{
		while ( getline(myfile, line) )	// We scan config.txt until...
		{
			if (line.find(("Graph color " + PMT_or_TGC).c_str()) != string::npos)	// ... we stumble upon the line containing Graph color <PMT_or_TGC>
			{
				color = line[line.length()-1] - '0';	// Color becomes the last character of that line (which is a number, we convert it to an int using a technique I found on stackoverflow here http://stackoverflow.com/a/5030086)
			}
		}
	}
}


void output_commands_to_root(vector<string> sensor_graphs, vector<string> sensor_graphs_draw, vector<string> cor_graphs, vector<string> cor_graphs_draw, vector<pair<string, string> > HV_graphs, vector<pair<string, string> > HV_graphs_draw, vector<pair<string, string> > HV_graphs_extrapol, bool only_24h)
{// This function will write the macro that will be used by ROOT to draw the graphs made by the user. It uses the the rootfile that resulted from the execution of the application.
	if (sensor_graphs_draw.size() || cor_graphs_draw.size() || HV_graphs_draw.size())
	{
		ofstream output("root_commands.c");
		if (output.is_open())
		{
			output << "#include <TMath.h>\n";
			output << "#include <TLegend.h>\n";
			output << "#include <ctime>        // struct std::tm\n\n";
			output << "void root_commands(string save_format)\n";
			output << "{\n";
			output << "\tcout << \"Receiving instructions. Now building graphs.\" << endl;\n";
			output << "\tgStyle->SetOptStat(0);\n";
			if (cor_graphs_draw.size()) output << "\tset_plot_style();\n";	// This line is for the correlation graphs!


			output << "\tTFile *myresults = new TFile(\"" << rootfile_name << "\", \"OPEN\");\n";
			output << "\tif (myresults->IsOpen())\n";
			output << "\t{\n";
			if (sensor_graphs.size())
			{
				output << "\t\t// We find min and max values for the yaxis of each requested graph and compare them to the values defined in config.txt\n";
				output << "\t\t//(Technique found here: https://root.cern.ch/root/roottalk/roottalk07/0134.html)\n";
				for (size_t i=0; i<sensor_graphs.size(); ++i)	// For each graph we fetched data for
				{	// These lines were set as a comment on January 13 2016
					output << "\t\tdouble min_" << sensor_graphs.at(i) << " = TMath::MinElement(" << sensor_graphs.at(i) << "->GetN()," << sensor_graphs.at(i) << "->GetY());\t\n";
					output << "\t\tdouble max_" << sensor_graphs.at(i) << " = TMath::MaxElement(" << sensor_graphs.at(i) << "->GetN()," << sensor_graphs.at(i) << "->GetY());\n";
					vector<string> sensor_info = yaxis_title(sensor_graphs.at(i),0,0);	// We fetch the sensor info from the config.txt
					output << "\t\tif (min_" << sensor_graphs.at(i) << " < " << sensor_info.at(2) << ") min_" << sensor_graphs.at(i) << " = " << sensor_info.at(2) << ";\n";
					output << "\t\tif (max_" << sensor_graphs.at(i) << " > " << sensor_info.at(3) << ") max_" << sensor_graphs.at(i) << " = " << sensor_info.at(3) << ";\n";
				}
			}
			if (sensor_graphs_draw.size())
			{
				output << "\t\t// THIS SECTION IS USED TO DRAW THE SENSOR GRAPHS (NOT correlation graphs)\n";
				for (size_t i=0; i < sensor_graphs_draw.size(); ++i)	// For each selected sensor...
				{
					vector<string> sensor_info = yaxis_title(sensor_graphs_draw.at(i),0,0);	// We fetch the sensor info from the config.txt
					output << "\t\t" << sensor_graphs_draw.at(i) << "->SetMarkerColor(" << sensor_info.at(4) << ");\n";
					output << "\t\t" << sensor_graphs_draw.at(i) << "->SetMarkerSize(0.5);\n"; 
					output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetLabelSize(0.025);\n";
					output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetLabelOffset(0.02);\n";
					output << "\t\t" << sensor_graphs_draw.at(i) << "->GetYaxis()->SetRangeUser(min_" << sensor_graphs_draw.at(i) << ",max_" << sensor_graphs_draw.at(i) << ");\n";


					output << "\n\t\t//HERE WE SET THE DEFAULT Y-AXIS RANGE DEFINED IN CONFIG.TXT. (The full graph is in " << rootfile_name << ")\n";
					output << endl;
					output << "\t\tTCanvas *c" << i << " = new TCanvas(\"" << sensor_graphs_draw.at(i) << "\", \"" << sensor_graphs_draw.at(i) << "\");\n";	
					output << "\t\tc" << i << "->SetGrid();	//Set the grid to enhance viewing experience\n";
					output << "\t\t" << sensor_graphs_draw.at(i) << "->Draw(\"ap\");\n";
					output_time_of_execution(output, 0);
					
					if (automatic)
					{
						// We want to output graphs with an x-axis rane of a week and a day (we use the month-long graph and modify its scale)
						time_t start_time;
						time_t end_time;
						get_now_time(end_time);	//end_time is now


						// To save execution time, we choose when we want to save month graph and week graph
						if (!only_24h)
						{
							start_time=end_time-31*24*3600;	// start_time is 1 month before now
							// This is ugly and I want it in write_limits_commands function, but it is to be added
							output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetLimits(" << start_time << "," << end_time << ");\n";		// Do NOT remove (SetRangeUser does not take effect (strangely) if SetLimits is not present beforehand)
							output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetRangeUser(" << start_time << "," << end_time << ");\n";
							output << "\t\t" << "if (save_format.size()) c" << i <<  "->SaveAs((\"/imports/WWW/people/" << user_save_folder << "/SCATool/sensors_graphs/month/" << sensor_graphs_draw.at(i) << "_month.\" + save_format).c_str());\n";
							start_time=end_time-7*24*3600;	// Start time is 1 week before now
							write_limits_commands(output, sensor_graphs_draw.at(i), start_time, end_time, "week", sensor_info.at(4), "sensors_graphs", i, "c");
						}

						start_time=end_time-24*3600;	// Start time is 1 day before now
						write_limits_commands(output, sensor_graphs_draw.at(i), start_time, end_time, "day", sensor_info.at(4), "sensors_graphs", i, "c");
					}
					else 
					{
						output << "\t\t// Set time range of the sensor graphs\n";
						// This is in case start_time_global == end_time_global == 0, if we asked to plot all data and they were not set. With this if, the root macro will not bug.
						if (start_time_global < end_time_global)
						{
							output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetLimits(" << start_time_global << "," << end_time_global << ");\n";	// Do NOT remove (SetRangeUser does not take effect (strangely) if SetLimits is not present beforehand)
							output << "\t\t" << sensor_graphs_draw.at(i) << "->GetXaxis()->SetRangeUser(" << start_time_global << "," << end_time_global << ");\n";	
						}
						output << "\t\t" << "if (save_format.size()) c" << i <<  "->SaveAs((\"" << sensor_graphs_draw.at(i) << ".\" + save_format).c_str());\n";
					}
				}
			}

			if (HV_graphs_draw.size())
			{
				output << "\n\t\t// BEGIN HV GRAPHS\n";

				for (size_t i=0; i<HV_graphs_draw.size(); ++i)
				{
					string line = HV_graphs_draw.at(i).first.substr(0,3);
					int color=0;
					get_HV_color(line, color);
					output << "\t\t//Get pointer to the current style (ATLAS)\n";
					output << "\t\tTStyle *style = gROOT->GetStyle(\"ATLAS\");\n";
					output << "\t\tif (style)\t//Tests if the user has ATLAS template installed\n\t\t{\n";
					output << "\t\t\t//Modify tick marks\n";
					output << "\t\t\tstyle->SetPadTickY(0); // does not put right tick marks in the graphs (we want this for the HV because of the TGaxis)\n";
					output << "\t\t}\n";
					output << "\n\n\t\t//An HV graph: " << HV_graphs_draw.at(i).first << "_imon\n";
					output << "\t\t" << HV_graphs_draw.at(i).first << "->GetXaxis()->SetTimeOffset(0, \"UTC\");\n";
					output << "\t\t" << HV_graphs_draw.at(i).first << "->GetXaxis()->SetLabelSize(0.025);\n";
					output << "\t\t" << HV_graphs_draw.at(i).first << "->GetXaxis()->SetLabelOffset(0.02);\n";
					output << "\t\t" << HV_graphs_draw.at(i).first << "->SetMarkerSize(0.5);\n";
					output << "\t\t" << HV_graphs_draw.at(i).second << "->SetMarkerColor(" << color << ");\n";
					output << "\t\t" << HV_graphs_draw.at(i).second << "->SetMarkerSize(0.5);\n";

					output << "\t\t//Legend\n";
					output << "\t\tTLegend *leg_" <<  HV_graphs_draw.at(i).first << " = new TLegend(0.16, 0.87, 0.5, 0.95);\n";
					
					output << "\t\tleg_" << HV_graphs_draw.at(i).first << "->AddEntry(" << HV_graphs_draw.at(i).first << ", \"" << HV_graphs_draw.at(i).first  << "\", \"p\");\n";
					output << "\t\tleg_" <<  HV_graphs_draw.at(i).first << "->AddEntry(" << HV_graphs_draw.at(i).second << ", \"" << HV_graphs_draw.at(i).second  << "\", \"p\");\n";
					//output << "\t\tleg_" << HV_graphs_draw.at(i).first << "->SetFillStyle(0);\t// Makes legend transparant, can be modified.\n";
					output << "\t\tleg_" << HV_graphs_draw.at(i).first << "->SetBorderSize(1);\n";

					// This scales the imon data points so that they are more visible in the final graph. We do it ONLY ONCE for each graph.
					output_scale_HV_imon(output, HV_graphs_draw.at(i).first, HV_graphs_draw.at(i).second);
					if (automatic)
					{
						// We want to output graphs with an x-axis rane of a week and a day (we use the month-long graph and modify its scale)
						time_t start_time;
						time_t end_time;
						get_now_time(end_time);	//end_time is now


						// To save execution time, we choose when we want to save month graph and week graph
						if (!only_24h)
						{
							start_time=end_time-31*24*3600;	// start_time is 1 month before now
                                                        output << "\n\n\t\t// Month " << HV_graphs_draw.at(i).first << "_imon\n";
							fill_HV_canvas(output, i, "month", HV_graphs_draw.at(i), HV_graphs_extrapol, start_time, end_time, color, 1);

							start_time=end_time-7*24*3600;	// Start time is 1 week before now
                                                        output << "\n\n\t\t// Week " << HV_graphs_draw.at(i).first << "_imon\n";
							fill_HV_canvas(output, i, "week", HV_graphs_draw.at(i), HV_graphs_extrapol, start_time, end_time, color, 2);
						}

						start_time=end_time-24*3600;	// Start time is 1 day before now
						output << "\n\n\t\t// Day " << HV_graphs_draw.at(i).first << "_imon\n";
						fill_HV_canvas(output, i, "day",  HV_graphs_draw.at(i), HV_graphs_extrapol, start_time, end_time, color, 3);
					}
					else 
					{
						fill_HV_canvas(output, i, "", HV_graphs_draw.at(i), HV_graphs_extrapol, 0, 0, color, 1);
					}
				}
			}
			if (cor_graphs_draw.size())
			{
				output << "\n\t\t// THIS SECTION IS USED TO DRAW THE CORRELATION GRAPHS\n";
				// For each correlation graph, we have 4 dimensions in the cor_graphs_draw vector. We want to draw every cor_graph.
				for (size_t i=0; i < cor_graphs_draw.size(); i+=4)
				{
					output << "\t\tTCanvas *cc" << i/4 << " = new TCanvas(\"" << cor_graphs_draw.at(i) << " VS " << cor_graphs_draw.at(i+1) << "\", \"" << cor_graphs_draw.at(i) << " VS " << cor_graphs_draw.at(i+1) << "\");\n";
					output << "\t\tcc" << i/4 << "->Divide(2,2);\n";

					output << "\n\t\t// First quadrant of canvas\n";
					output << "\t\tcc" << i/4 << "->cd(1);\n";
					output << "\t\tcc" << i/4 << "->cd(1)->SetGrid();\n";
					vector<string> sensor_info1 = yaxis_title(cor_graphs_draw.at(i),0,0);	// We fetch the sensor info from the config.txt
					output << "\t\t" << cor_graphs_draw.at(i) << "->SetMarkerColor(" << sensor_info1.at(4) << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i) << "->SetMarkerSize(0.5);\n";
					output << "\t\t" << cor_graphs_draw.at(i) << "->GetXaxis()->SetLabelSize(0.025);\n";
					output << "\t\t" << cor_graphs_draw.at(i) << "->GetXaxis()->SetLabelOffset(0.02);\n";

					output << "\t\t// Set graphic ranges (min_ max_ values between time interval)\n";
					output << "\t\t" << cor_graphs_draw.at(i) << "->GetYaxis()->SetRangeUser(min_" << cor_graphs_draw.at(i) << ", max_" << cor_graphs_draw.at(i) << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i) << "->Draw(\"ap\");\n";

					output << "\n\t\t// Second quadrant of canvas\n";
					output << "\t\tcc" << i/4 << "->cd(2);\n";
					output << "\t\tcc" << i/4 << "->cd(2)->SetGrid();\n";
					vector<string> sensor_info2 = yaxis_title(cor_graphs_draw.at(i+1),0,0);	// We fetch the sensor info from the config.txt
					output << "\t\t" << cor_graphs_draw.at(i+1) << "->SetMarkerColor(" << sensor_info2.at(4) << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i+1) << "->SetMarkerSize(0.5);\n";
					output << "\t\t" << cor_graphs_draw.at(i+1) << "->GetXaxis()->SetLabelSize(0.025);\n";
					output << "\t\t" << cor_graphs_draw.at(i+1) << "->GetXaxis()->SetLabelOffset(0.02);\n";
					output << "\t\t// Set graphic ranges (min_ max_ values between time interval)\n";
					output << "\t\t" << cor_graphs_draw.at(i+1) << "->GetYaxis()->SetRangeUser(min_" << cor_graphs_draw.at(i+1) << ", max_" << cor_graphs_draw.at(i+1) << ");\n";

					output << "\t\t" << cor_graphs_draw.at(i+1) << "->Draw(\"ap\");\n";

					output << "\n\t\t// Third quadrant of canvas\n";
					output << "\t\tcc" << i/4 << "->cd(3);\n";
					output << "\t\t" << cor_graphs_draw.at(i+2) << "->Draw(\"colz\");\n";

					output << "\n\t\t// Fourth quadrant of canvas\n";
					output << "\t\tcc" << i/4 << "->cd(4);\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->SetMarkerColor(";
					if (sensor_info1.at(4) == sensor_info2.at(4))	// Sensor_<name1> and Sensor_<name2> have same marker color. Applying color to TGraph2D in ROOT macro. 
					{
						output << sensor_info1.at(4);
					}
					else output << "4"; 
					output << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->SetMarkerSize(0.5);\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetLabelSize(0.025);\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetLabelOffset(0.02);\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->SetTitle(\"Correlation Graph between " << cor_graphs_draw.at(i) << ", " << cor_graphs_draw.at(i+1) << " and time\");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetTimeOffset(0, \"UTC\");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetTimeDisplay(1);	//Display time axis\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetTitle(\"Time #splitline{(YY/MM/DD)}{(hh:mm:ss)}\");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetTimeFormat(\"#splitline{%Y/\%m/\%d}{\%H:\%M:\%S}\");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetTimeOffset(0, \"UTC\");\n";
					output << "\t\tstring line = " << cor_graphs_draw.at(i) << "->GetYaxis()->GetTitle();\n"; 
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetYaxis()->SetTitle(line.c_str());\n";
					output << "\t\tline = " << cor_graphs_draw.at(i+1) << "->GetYaxis()->GetTitle();\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetZaxis()->SetTitle(line.c_str());\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetYaxis()->SetRangeUser(min_" << cor_graphs_draw.at(i) << ", max_" << cor_graphs_draw.at(i) << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetZaxis()->SetRangeUser(min_" << cor_graphs_draw.at(i+1) << ", max_" << cor_graphs_draw.at(i+1) << ");\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->GetXaxis()->SetLabelOffset(0.02);\n";
					output << "\t\t" << cor_graphs_draw.at(i+3) << "->Draw(\"p\");\n";
					output << "\t\tcc" << i/4 << "->cd();\n";
					output << "\t\t//We create a transparent pad on top of the others, see https://root.cern.ch/phpBB3/viewtopic.php?t=17862\n";
					output << "\t\tTPad *newpad=new TPad(\"newpad\",\"a transparent pad\",0.30,0.47,0.70,0.53);\n";
					output << "\t\tnewpad->SetFillStyle(4000);\n";
					output << "\t\tnewpad->Draw();\n";
					output << "\t\tnewpad->cd();\n";
					output_time_of_execution(output, 1);

					if (!automatic)
					{
						output << "\t\tif (save_format.size()) cc" << i/4 << "->SaveAs((\"Cor_" << cor_graphs_draw.at(i) << "_" << cor_graphs_draw.at(i+1) << ".\" + save_format).c_str());\n";
					}
				}
			}
			output << "\t}\n";
			output << "\telse cout << \"Could not open " << rootfile_name << "\" << endl;\n";
			output << "}\n";

			if (cor_graphs_draw.size())
			{
				output << "\n// Taken from http://ultrahigh.org/2007/08/making-pretty-root-color-palettes/\n";
				output << "void set_plot_style()\n";
				output << "{\n";
				output << "\tconst Int_t NRGBs = 5;\n";
				output << "\tconst Int_t NCont = 255;\n\n";
				output << "\tDouble_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };\n";
				output << "\tDouble_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };\n";
				output << "\tDouble_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };\n";
				output << "\tDouble_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };\n";
				output << "\tTColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);\n";
				output << "\tgStyle->SetNumberContours(NCont);\n";
				output << "}\n";
			}
		}
		cout << "Created root macro \"root_commands.c\"\n\n";
	}
	else // No graphs were asked to be drawn
	{
		if (fexists("root_commands.c"))	// If there is a pre-existing root_commands.c file, from an older execution
		{
			if (sensor_graphs.size() || cor_graphs.size() || HV_graphs.size())	// If user requested we draw at least one graph
			{
				system("rm root_commands.c");	// We delete old (out of date) root_commands.c to prevent any conflicts.
				cout << "Info: You requested no graphs to be drawn. SCATool therefore deleted the pre-existing root_commands.c file to prevent eventual conflicts.\n";
			}
		}
	}
}


void get_now_time(time_t &end_time)
{// This function  gives the time of execution of the scatool in seconds since year 1900
	time_t rawtime;
	struct tm * timeinfo;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
	end_time=mktime (timeinfo);	// end time is now

}

void one_month_range(time_t &start_time, time_t &end_time)
{// This function will modify start_time_global and end_time_global so that we have a month range ending on the time of execution of the scatool (this app)
	get_now_time(end_time);
	start_time=end_time-31*24*3600;	// Start time is 1 month before now
}

void output_time_of_execution(ofstream & output, bool cor)
{	// Will output in root_commands.c the code to write the time of execution text
	output << "\t\t" << "TText *text = new TText();\n";
	output << "\t\t" << "text->SetNDC();\n";	
	output << "\t\t" << "text->SetTextFont(1);\n";
	output << "\t\t" << "text->SetTextSize(0.";
	if (!cor) output << "0";	// If we do not draw a correlation graph, we want size 0.03. (because correlation graphs put text on a smaller pad, the text size has to be bigger)
	output << "3);\n";
	output << "\t\t" << "text->SetTextAlign(22);\n";
	output << "\t\t" << "text->SetTextAngle(0);\n";
	output << "\t\t" << "text->DrawText(";
	if (cor) output << "0.5,0.5";	// If correlation graph, we want "generated on.." text to be full center
	else output << "0.3,0.05";	// For non correlation graphs, we want it bottom left.
	output << ",\"" << generated_on() << "\");\n";
}

void time_of_execution(time_t end_time)
{
	// Gives time of execution of the scatool (used for the cronjob)
	cout << "Time of execution: " << from_seconds_to_date(end_time) << endl;
}

void data_to_graph(vector<string> &sensor_graphs, vector<string> &sensor_graphs_draw)
{// This function asks the user for which sensor they want to plot, then proceeds to parse the data and write it in a TGraph
////////// SELECT SENSORS FOR ANALYSIS ////////////
	string sensor_request;
	bool draw_or_not1=0;	// Draw or not for sensor graphs
	string upd_or_recr;
	vector<string> requests_list;
	string filename;
	vector<string> sensor_list;

	system("ls -d /raid/aither/SlowControlData/Sensor_* > list_of_files");	//Will make list of folder names found in the SlowControlData repository inside file named "list_of_files"

	// When we run this tool automatically (i.e. with a cronjob), we want the time of execution to appear in the log
	if (automatic)
	{
		sensor_request="0";
		one_month_range(start_time_global, end_time_global);
		time_of_execution(end_time_global);
		goto inside_while_loop;
	}
	else
	{	
		ifstream names ("list_of_files");	// Open list_of_files to read inside it

		// Enter all of the sensors listed in list_of_files into a vector called sensor_list
		while ( getline(names,filename) )
		{
			filename.erase(0,29);	// Remove the pathname /raid/aither/SlowControlData/
			sensor_list.push_back(filename);
		}
		cout << "\n\tList of available sensors:\n\t..................................................\n";
		for (size_t s=0; s<sensor_list.size(); ++s)
		{
			cout << "\t" << s+1 << ")\t" << sensor_list.at(s) << endl;
		}
		cout << "\t0)\tALL\n\t..................................................\n";

		cout << "Select the number of a sensor you want to analyze\nSelection #: ";
	}

	while (getline(cin, sensor_request) && sensor_request.compare("done"))	// We keep selecting sensors until the using says they want ALL sensors, or type done
	{
		inside_while_loop:	// This is just so, if we use the automatic version, we can bypass the getline in the while loop
		size_t select_num = atoi(sensor_request.c_str());
		// We escape if the user said he was done
		if (sensor_request.empty())
		{
			if (0 < requests_list.size())
			{// If user says they are done and selected at least 1 sensor to analyze
				break;
			}
			else
			{
				cout << "Error: You must select a sensor (or type \"done\" if you are really done).\n";
			}
		}
		// Fill requests_list with all the names of the sensors
		if (sensor_request == "0")
		{
			cout << "Added ALL sensors to selection.\n";
			requests_list.clear();	// Empty previous selections if user selected anything before doing all
			ifstream files ("list_of_files");
			while (getline(files,filename))
			{
				// I want folders which only contain "Sensor_" in their title
				// Reference on technique http://www.cplusplus.com/reference/string/string/find/
				if (filename.find("/raid/aither/SlowControlData/Sensor_") != string::npos)
				{
					requests_list.push_back(filename);
				}
			}
			break;
		}

		else if (0 < select_num && select_num <= sensor_list.size())
		{
			if (new_vector_item(requests_list, "/raid/aither/SlowControlData/" + sensor_list.at(select_num-1)))
			{
				cout << "Added " << sensor_list.at(select_num-1) << " to selection.\n";
			}
			else 
			{
				cout << "Error: " << sensor_list.at(select_num-1) << " has already been selected.\n";
			}
		}
		cout << "Selection # ";
		if (requests_list.size())
		{
			cout << "(press \"enter\" to end): ";
		}
	}
	// We remove list_of_files, since it is not needed anymore
	system("if [ -e list_of_files ]\nthen rm list_of_files\nfi");

	if (automatic)
	{
		draw_or_not1=1;	// Draw or not for sensor graphs
		setrang_or_not=1;	// Set range or not for sensor graphs
		upd_or_recr = "recreate";	// Will automatically recreate a new rootfile (erase old one)		
	}

	else
	{
		ask_timerange(setrang_or_not);
		ask_draw(draw_or_not1);
		ask_new_rootfile(upd_or_recr);
	}
	cout << "Graphs will be saved into " << rootfile_name << " file. ";

	// Create the new root file
	TFile *rootfile = new TFile(rootfile_name.c_str(),upd_or_recr.c_str());

////////// ANALYZE FILES FOR EACH REQUESTED SENSOR //////////
	for (size_t i=0; i<requests_list.size(); ++i)	// For each selected sensor...
	{
		cout << "\nParsing " << replace_spaces_in_string(requests_list.at(i), "\\ ") << " data..." << endl;
		vector<vector<double> > data(2);// 1st dim: date. 2nd dim: measurements
		double min_value=1111111;	// Min value for graphs range (can be greater than ymin in config file)
		double max_value=-1111111;	// Max value for graphs range (can be greater then ymax in config file)
		vector<string> sensor_info = yaxis_title(requests_list.at(i),1,1);	// We fetch the sensor info from the config.txt
		

		// Will create a .sh file that will list directory and output 
		//it in a text file that will be read by this code
		create_list_directory(replace_spaces_in_string(requests_list.at(i), "\\ "));

		// Now we have all folder names stored in data_files.txt
		ifstream data_files_list ("data_files");
		string data_file_name;
		vector<string> data_files_vector;
		int points_outside=0;	// Counts how many points are outside y-axis boundaries defined in config.txt

		// Opens data files one at a time
		while (getline(data_files_list, data_file_name))
		{	
			// Here we filter files (remove Trans_, Conflict, desktop.ini)
			if ( filename_check(data_file_name) )
			{
				// Here we filter files which are outside the specified time interval
				if ( data_in_range(setrang_or_not, data_file_name, start_time_global, end_time_global) )
				{
					data_files_vector.push_back(data_file_name);	//Keep in memory the data file names to output the oldest and newest measurement files once the sensor data is read
					ifstream data_file ((requests_list.at(i) + "/" + data_file_name).c_str());
					if (data_file.is_open())
					{

						string date_time;
						string data_line;

						//Skip the first line
						data_file.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );
						while (getline(data_file, data_line))
						{
							//Get the time of measurement (http://goo.gl/G2h83p)
							bool is_time_ok=0;
							date_time = data_line.substr(0,19);
							struct tm when;
							time_t converted_time;

							// We verify if time is within range
							// This function also fills variable "converted_time"
							f_is_time_ok(   is_time_ok, 
									setrang_or_not, 
									when, 
									converted_time, 
									start_time_global, 
									end_time_global, 
									requests_list.at(i) + "/" + data_file_name,
									date_time
								    );

							// We verify if the measurement has been taken inside the specified time interval
							if (is_time_ok)
							{

								data_line.erase(0,20);	// Remove date from data_line, leaves only measurement

								double data_value = atof(data_line.c_str());

								data.at(0).push_back(converted_time);
								data.at(1).push_back(data_value);
								// We search for minimum value in graph, and it can be outside the default range from config
								if (data_value < min_value) 
								{
									min_value = data_value;
								}

								if (max_value < data_value) 
								{
									max_value = data_value;
								}
								// If data_value is outside range in config.txt
								if (atof(sensor_info.at(2).c_str()) > data_value || data_value > atof(sensor_info.at(3).c_str()))
								{
									points_outside++;	// To count how many poitns are outside viewing range (set in config)
								}
							}
						}
					}
					// If the file didn't open for some reason I did not predict
					else	cout << "\tThis file was unexpectedly omitted: " << data_file_name << endl;
				}
			}	
		}
		if (data_files_vector.size())
		{
			if (data.at(0).size())
			{// If we decide to draw the graphs, we add their names to a vector (only if graphs are not empty).
				string line=requests_list.at(i);
				line.erase(0,29);	// Will remove path name
				new_vector_item(sensor_graphs, replace_spaces_in_string(line, "_"));
				
				if(draw_or_not1)
				{
					new_vector_item(sensor_graphs_draw, replace_spaces_in_string(line, "_"));
				}

				// Will output which files are oldest and newest in the measurement data
				cout_oldest_newest_files(data_files_vector);

				cout << "\tmin value = " << min_value << " max value = " << max_value << endl; 
				// Remove the file we created that contained the data file names inside de Sensor folder (created by create_list_directory function)
				system("if [ -e data_files ]\nthen rm data_files\nfi");

				TGraph *sensor_graph = new TGraph (data.at(0).size(), &(data.at(0)[0]), &(data.at(1)[0]));

				
				sensor_graph->GetXaxis()->SetTimeDisplay(1);	//Display time axis
				sensor_graph->GetXaxis()->SetTitle("Time #splitline{(YY/MM/DD)}{(hh:mm:ss)}");
				sensor_graph->GetXaxis()->SetTimeFormat("#splitline{%Y/\%m/\%d}{\%H:\%M:\%S}");
				sensor_graph->GetXaxis()->SetTimeOffset(0, "UTC");

				sensor_graph->SetName(replace_spaces_in_string(sensor_info.at(0), "_").c_str());
				sensor_graph->GetYaxis()->SetTitle(sensor_info.at(1).c_str());
				sensor_graph->Write();	// Save the graph to the rootfile
			}
			else 
			{
				cout << "** Info **: Graph for " << replace_spaces_in_string(sensor_info.at(0).c_str(), "_") << " contains no data and therefore cannot be drawn.\n";
				cout << "(Tip: Try using a larger time range which might contain data)\n";
				cout << endl;
			}
			if (points_outside)
			{
				{
					cout << "\tInfo: " << points_outside << " points are outside default y-axis viewing range.\n"; 
				}
			}
		}
		else if (setrang_or_not) 
		{
			cout << "\n** Info **: No data found inside specified time range for "<< sensor_info.at(0) << ". No graph is produced.\n";
		}
		else cout << "Error: Unexpected! No data found.\n";
	}
	cout << "\nChanges added to \"" << rootfile_name << "\" file." << endl;
	delete rootfile;
}


void correlation_graph(vector<string> &sensor_graphs, vector<string> &cor_graphs, vector<string> &cor_graphs_draw)
{// This function will ask the user which correlation graphs they want to produce, then proceeds to create the correlation graphs.
	if (automatic) cout << "Now drawing correlation graphs\n";
	if (TFile::Open(rootfile_name.c_str()))
	{
		// Open ROOT file generated by sensor-analysis.cpp (the rootfile) in "append" mode (i.e. update)
		TFile *file = new TFile(rootfile_name.c_str(), "update");
		if ( file->IsOpen() ) 
		{
			cout << "\nFile " << rootfile_name << " opened successfully. Here are its contents:\n----------------------------------\n";
			file->ls();	// Will list graphs contained in the ROOT file
			cout << "----------------------------------\n\n";
			string line="n";;
			if (1 < sensor_graphs.size())
			{
				cout << "Do you want to automatically compute every possible correlation graph from this list (for Sensor_<name> graphs only) (y/n): ";
				while (1)
				{
					getline(cin,line);
					if (line == "y" || line == "n") break;
					else cout << "Invalid choice. Try again. (y/n)\n";
				}
			}
			else if (1 == sensor_graphs.size()) cout << "Info: Automatic selection mode becomes available if you have selected more than 1 sensor.\n\n";
			else 
			{
				cout << "Info: Automatic selection mode becomes available if you select option 1) from the menu before option 2) in the same execution. File \"" << rootfile_name << "\" must also already contain Sensor_<name> graphs.\n";
				// TO BE PATCHED:
				// I made a "rootfile_contains(string, string)" function that can tell if a rootfile contains a graph with a certain name. 
				// Using this function, or creating a new similar one, I am pretty sure we can acheive the automatic process of correlating every graph
				// even if sensor_graphs is empty (sensor_graphs will always be empty if, for example, a user starts the program
				// and possesses a "rootfile_name" in their directory and proceeds to select option 2 in the menu).					
			}
			char auto_cor = line[0];
			if (auto_cor=='n')
			{
				cout << "\n---Manual selection mode--- (you can select different settings for each correlation graph)\n";
				while (1)
				{
					// Asks user which graphs they want drawn
					string first_name;
					string secnd_name;

					while (1)
					{
						cout << "\nSelect first graph (type \"done\" to exit): Sensor_";
						getline(cin, first_name);
						if (first_name == "done") goto end_selection;// if user is done selecting graphs for correlation, we exit both while loops
						if (find_graph_in_root(file,first_name)) break;
					}
					while (1)
					{
						cout << "Select second graph (type \"done\" to exit): Sensor_";
						getline(cin, secnd_name);
						if (secnd_name == "done") goto end_selection;
						if (find_graph_in_root(file,secnd_name)) break;
					}
					fill_correlation_graphs(first_name, secnd_name, file, cor_graphs, cor_graphs_draw, sensor_graphs, 1);
				}
				end_selection: ;
			}
			else if (auto_cor=='y') goto automatic_selection;
			if (automatic)
			{
				automatic_selection:

				for (size_t i=0; i<sensor_graphs.size(); ++i)
				{
					for (size_t j=0; j<sensor_graphs.size(); ++j)
					{
						if (i<j)
						{
							string first_name = sensor_graphs.at(i);
							first_name.erase(0,7);	// remove "Sensor_" because it is hard coded in function fill_correlation_graphs
							string secnd_name = sensor_graphs.at(j);
							secnd_name.erase(0,7);
							fill_correlation_graphs(first_name, secnd_name, file, cor_graphs, cor_graphs_draw, sensor_graphs, 0);
						}
					}
				}
			}
		}
		// Close the rootfile
		delete file;
	}
	//If the rootfile did not open
	else 
	{
		cout << "Could not open file " << rootfile_name << " ";
		if (!automatic) cout << "(Tip: Try selecting #1 in the menu before selecting #2?)";
		cout << "\n";
	}
}

void extrapolate_HV_vmon_imon(TGraph* HV_graph, TGraph* HV_extrapol, vector<bool> on_off_vv)
{// This function calls scan_HV_vmon_imon to do the extrapolation into HV_extrapol. It determines what are to two adjacent data points to be analyzed.
	int N_value = HV_graph->GetN();	// Number of points in the graph (vmon or imon)
	for (int n=0; n < N_value-1; ++n)	//N_value-1, because e.g. if there are 3 points in graph, there are 2 gaps where potential extrapolation can happen.
	{
		scan_HV_vmon_imon(HV_graph, HV_extrapol, on_off_vv, n);	// This function extrapolates the straight lines into HV_extrapol after reading HV_graph
	}
	extrapolate_extremity_HV_graph(HV_graph, HV_extrapol, on_off_vv, start_time_global, 0);
	extrapolate_extremity_HV_graph(HV_graph, HV_extrapol, on_off_vv, end_time_global, 1);
}

void extrapolate_extremity_HV_graph(TGraph* HV_graph, TGraph* HV_extrapol, vector<bool> on_off_vv, time_t reference_time, bool mode)
{// this function extrapolates from the last point of an HV graph (vmon or imon) to the specified end_time (which should be the rightmost edge of the graph). This is mainly used for extrapolations for the automatic monitoring, but might see implementation in the manual use as well.
// the current plan for this function is to use it on monthly graph (in the automatic monitoring). That way, when we SetLimits() on the monthly graph so that it becomes a weekly graph (and finally another SetLimits() so that it becomes a daily graph), we will have an extrapolation inside them, even if no actual data point was taken. The extrapolation should let us know in real time if there is something on or not in the HV crate.
	int N_pts_graph = HV_graph->GetN();
	if (N_pts_graph)	// If the graph contains points
	{
		time_t measure_gap_secs = 0; 
		time_t min_gap_extrapol = 10800;
		{
			if (mode==0)	//Plot beginning of graph extrapolation
			{
				double first_x, first_y;
				HV_graph->GetPoint(0,first_x, first_y);
				measure_gap_secs = abs(reference_time - first_x);	// Gives the absolute time difference between the leftmost edge of the graph (initial time) and the first x point.
				if (on_off_vv.at(0) == 1 && 10 < first_y)	// We only do the extrapolation if the bit is "on" and the first_y is bigger than 5 (we don't want to do an extrapolation while it is in ramp up
				{
					if (min_gap_extrapol <= measure_gap_secs)	// If the gap is large enough to warrant an extrapolation (3 hours)
					{
						extrapolate(measure_gap_secs, min_gap_extrapol, HV_graph, HV_extrapol, reference_time, first_y);
					}
				}
			}
			if (mode==1)	//Plot end of graph extrapolaton
			{
				double last_x, last_y;
				HV_graph->GetPoint(N_pts_graph-1, last_x, last_y);
				measure_gap_secs = abs(reference_time - last_x);	// Gives the absolute time difference between the rightmost edge of the graph (final time) and the last x point. If this gap is big enough and the point at last_x had an "on" bit, it means the system is still running and needs extrapolation (because no data is recorded when the system is stable).
				if (on_off_vv.at(N_pts_graph - 1) == 1 && 10 < last_y) // We only do the extrapolation if the bit is "on" and the first_y is bigger than 5 (we don't want to do an extrapolation if it is in ramp down)
				{
					if (min_gap_extrapol <= measure_gap_secs)	// If the gap is large enough to warrant an extrapolation (3 hours)
					{
						extrapolate(measure_gap_secs, min_gap_extrapol, HV_graph, HV_extrapol, last_x, last_y);
					}
				}
			}
		}
	}
}

void scan_HV_vmon_imon(TGraph* HV_graph, TGraph* HV_extrapol, vector<bool> on_off_vv, int n)
{// This function reads two adjacent points in HV_graph and decides if there must be an extrapolation in HV_extrpaol
	time_t previous_time = HV_graph->GetX()[n];
	time_t current_time = HV_graph->GetX()[n+1];
	time_t measure_gap_secs = current_time - previous_time;	// Gives time difference between two adjacent points of interest (ref: https://root.cern.ch/phpBB3/viewtopic.php?t=5946)

	// HV does not record measurements if value does not change. We fill the graph with missing values if we know the HV was on for easier reading of the graph
	if (on_off_vv.at(n) == on_off_vv.at(n+1) && on_off_vv.at(n) == 1)	// If the machine was on at the previous and current point (don't want to extrapolate off time)
	{
		time_t min_gap_extrapol = 10800;
		if (min_gap_extrapol <= measure_gap_secs)	// If there was enough time between two measurements to make an extrapolation (we aim to plot an extrapolation point every 3 hours) (299 300 for future ref and searches)
		{
			if (previous_time !=0)
			{
				extrapolate(measure_gap_secs, min_gap_extrapol, HV_graph, HV_extrapol, previous_time, HV_graph->GetY()[n]);
			}
		}
	}
}

void extrapolate(time_t measure_gap_secs, time_t min_gap_extrapol, TGraph *HV_graph, TGraph *HV_extrapol, time_t extrapol_start_time, double y)
{
	for (int i=0; i <= measure_gap_secs/min_gap_extrapol; i++)	
	{
		int N_extrapol = HV_extrapol->GetN();
		HV_extrapol->SetPoint(N_extrapol, extrapol_start_time + i*min_gap_extrapol, y);
	}
}

void set_HV_axis(TGraph *hv_graph, string title)
{//I created this function because I needed to set the axis for HV graphs like this multiple times
	hv_graph->GetXaxis()->SetTimeDisplay(1);	//Display time axis
	hv_graph->GetXaxis()->SetTitle("Time #splitline{(YY/MM/DD)}{(hh:mm:ss)}");
	hv_graph->GetXaxis()->SetTimeFormat("#splitline{%Y/\%m/\%d}{\%H:\%M:\%S}");
	hv_graph->GetXaxis()->SetTimeOffset(0, "UTC");
	hv_graph->GetYaxis()->SetTitle(title.c_str());
	if (hv_graph->GetN()) // If the graph contains points
	{
		hv_graph->Write();	// Will write TGraph to rootfile. 
	}
}

void analyze_HV(vector<pair<string, string> > &HV_graphs, vector<pair<string, string> > &HV_graphs_draw, vector<pair<string, string> > &HV_graphs_extrapol)
{// This function looks for the data of every board_channel configuration inside a time range given by the user. All graphs are saved to the rootfile. The program then asks the user which graph they want to be explicitely drawn using the root macro. 
	vector<string> folder_names_vector;
	bool draw_or_not=0;
	string upd_or_recr;	// String we will add to TFile declaration to recreate or simply update an existing rootfile
	if (automatic)	// If we use an automatic version of the SCATool (for cronjob)
	{
		draw_or_not=1;	// Draw or not for sensor graphs
		setrang_or_not=1;	// Set range or not for sensor graphs
		upd_or_recr = "update";	// Willupdate rootfile (in automatic version, rootfile is recreated by data_to_graph function (sensor_graphs)) 
		one_month_range(start_time_global, end_time_global);
		time_of_execution(end_time_global);
	}
	else
	{
		ask_timerange(setrang_or_not);
		ask_draw(draw_or_not);	//TO-DO: remove this? 
		ask_new_rootfile(upd_or_recr);
	}
	system("ls -d /raid/aither/SlowControlData/HV/2* > list_of_files_HV"); // Will list directories beginning with 2 (year 2000, this means my code will work until year 3000!)
	ifstream files ("list_of_files_HV");
	string folder_name_path;	// Name of date directory (i.e. i/raid/aither/SlowControlData/HV/2015-01-23)
	map<string, pair<TGraph*, TGraph*> > graphs_HV_map;	// TGraphs of the HV data
	map<string, pair<TGraph*, TGraph*> > graphs_HV_extrapol_map;	// Contains the TGraphs of the extrapolations 
	map<string, vector<bool> > on_off_m;	//map of vectors of on/off bits
	vector<string> board_channel_vector;

	// Create the new root file
	TFile *rootfile = new TFile(rootfile_name.c_str(),upd_or_recr.c_str());
	cout << "Parsing /raid/aither/SlowControlData/HV/ data...\n";


	while (getline(files, folder_name_path))	// Loop over date folders
	{
		if ( data_in_range(setrang_or_not, folder_name_path, start_time_global, end_time_global))
		{
			string folder_date = folder_name_path.substr(32,10);
			folder_names_vector.push_back(folder_date);

			string data_file_name;	// Name of files in date directory (i.e. 6_0_2015-05-25.txt)
			create_list_directory(folder_name_path);	// creates data_files, contains files in a directory
			ifstream data_files_1_day ("data_files");
			while (getline(data_files_1_day, data_file_name))	// Loop over files of same date folder
			{
				int pos=0;
				pos=data_file_name.find('-')-4;	// Substract 4 because we want to start reading the string from the year number.
				string file_date = data_file_name.substr(pos,10);
				if (strcmp(file_date.c_str(), folder_date.c_str()) == 0)	// If file is in correct folder
				{
					// We actually start reading the data files here
					ifstream a_data_file ((folder_name_path + "/" + data_file_name).c_str());
					if (a_data_file.is_open())
					{
						string board_channel = data_file_name.substr(0,strpos(data_file_name, '_', 2)-1); // e.g. takes "1_11" from file name, which is exactly board_channel
						string pmt_or_tgc = decide_pmt_or_tgc(board_channel);

						// The following steps are so that it is clear in the HV graph name which number is graph and which is channel
						size_t underscore_position = board_channel.find("_");
						board_channel.insert(underscore_position+1, "channel");
						board_channel.insert(0, "board");


						map <string, pair<TGraph *, TGraph *> >::iterator it = graphs_HV_map.find(board_channel);	// ref: http://en.cppreference.com/w/cpp/container/map/find

						// We verify if we must declare a new set of TGraphs in our map
						if (it == graphs_HV_map.end())	// TGraphs for board_channel do not exist in map
						{
							new_vector_item(HV_graphs, make_pair(pmt_or_tgc + "_" + board_channel + "_vmon", pmt_or_tgc + "_" + board_channel + "_imon") );
							// A new TGraph is created for the board_channel
							TGraph *HV_graph_vmon = new TGraph ();
							TGraph *HV_graph_imon = new TGraph ();
							HV_graph_vmon->SetName((pmt_or_tgc + "_" + board_channel + "_vmon").c_str());
							HV_graph_imon->SetName((pmt_or_tgc + "_" + board_channel + "_imon").c_str());
							graphs_HV_map [board_channel] = make_pair(HV_graph_vmon, HV_graph_imon);	//Here we add the 2 TGraphs in our map
							// A new TGraph is created for the extrapolations of the board_channel
							TGraph *HV_graph_extrapol_vmon = new TGraph ();
							TGraph *HV_graph_extrapol_imon = new TGraph ();
							HV_graph_extrapol_vmon->SetName((pmt_or_tgc + "_" + board_channel + "_vmon_extrapol").c_str());
							HV_graph_extrapol_imon->SetName((pmt_or_tgc + "_" + board_channel + "_imon_extrapol").c_str());
							graphs_HV_extrapol_map [board_channel] = make_pair(HV_graph_extrapol_vmon, HV_graph_extrapol_imon);	//Here we add the 2 TGraphs in our map
						}

						//Skip the first line
						a_data_file.ignore ( std::numeric_limits<std::streamsize>::max(), '\n' );

						// We dissect every line from the data file
						string line;
						while ( getline(a_data_file, line) )
						{
							bool is_time_ok=0;
							string date_time = line.substr(0,19);
							struct tm when;
							time_t converted_time;

							// We check if measurements fall inside the requested time range
							// This function also fills variable "converted_time"
							f_is_time_ok(   is_time_ok, 
									setrang_or_not, 
									when, 
									converted_time, 
									start_time_global, 
									end_time_global, 
									folder_name_path + "/" + data_file_name,
									date_time
								    );
							if (is_time_ok)
							{
								// Note: j'ai fait une fonction qui permet d'enlever 25 ans rapidement a une valuer de time_t!
								int yy=0, mm=0, dd=0, hh=0, min=0, ss=0;
								double vmon=0;	// Will specify with scanf
								double imon=0;	// Will specify with scanf
								bool on_off=0;	// 1 == ON, 0 == OFF, specify with scanf
								int on_off_int=0;	// Because suggestion http://goo.gl/UkCun4 (issue with scanf and bool variables

								// Extracts time (with hour of measurement), vmon, imon and on_off bit from the data file. Super important line.
								sscanf(line.c_str(), "%d/%d/%d %d:%d:%d\t%*f\t%*f\t%lf\t%lf\t%*f\t%*f\t%*f\t%*f\t%d", &yy, &mm, &dd, &hh, &min, &ss, &vmon, &imon, &on_off_int);
								on_off = on_off_int;	// the bool variable becomes equal to the int variable (which fulfilled its use)
								on_off_m[board_channel].push_back(on_off);	// The vector of on/off bits for a board_channel configuration is stored in the map (historically called on_off_vv here)
								from_date_to_seconds(yy, mm, dd, hh, min, ss, converted_time);	// We convert the date into a time_t value stocked in converted_time

								// Filling the VMon graph with the data point we just got
								graphs_HV_map[board_channel].first->SetPoint(graphs_HV_map[board_channel].first->GetN(), converted_time, vmon);
								graphs_HV_map[board_channel].second->SetPoint(graphs_HV_map[board_channel].second->GetN(), converted_time, imon);
							}
						}
					}
				}
				else 
				{
					cout << "\tWarning: File \"" << data_file_name << "\" contains different date than directory \"" << folder_date << "\" (will will not be analyzed until error is fixed).\n";
				}
			}
		}
	}


	// Loop over all of the elements of the map containing the HV graphs. Taken from: http://stackoverflow.com/questions/4844886/how-to-loop-through-a-c-map
	cout << "Plotting HV extrapolations / Removing empty graphs...\n";
	typedef map <string, pair<TGraph *, TGraph *> >::iterator it_type;
	for (it_type iter_map = graphs_HV_map.begin(); iter_map != graphs_HV_map.end(); ++iter_map)
	{
		// If a graph contains no points, ...
		if (!iter_map->second.first->GetN() && !iter_map->second.second->GetN())
		{
			// ... we remove it from the HV_graphs vector	
			for (size_t tt=0; tt < HV_graphs.size() ; ++tt)
			{	// (Find the position in HV_graphs which contains the same string as the name of the HV graph)
				if (!HV_graphs.at(tt).first.compare(iter_map->second.first->GetName()))
				{
					HV_graphs.erase(HV_graphs.begin() + tt);
					break;
				}
			}
		}
		///////// EXTRAPOLATION ////////
		// NOTE: if you are confused about the abundance of 'first' and 'second': The first 'first (or second)' refers to the map, first = key, second = value.
		//	 The second 'first (or second)' refers to the element in the pair (element of the map). first = vmon, second =imon.
		//	 	{i.e., something like iter_map->second.first accesses the map element with the 'second' key (i.e. 'board_channel'). 
		//	 	{This element is a pair, so the '.first' accesses the first element of that pair (in our case, VMon).
		//	 If still confused, see http://stackoverflow.com/questions/4844886/how-to-loop-through-a-c-map
		extrapolate_HV_vmon_imon(iter_map->second.first, graphs_HV_extrapol_map[iter_map->first].first, on_off_m[iter_map->first]);
		extrapolate_HV_vmon_imon(iter_map->second.second, graphs_HV_extrapol_map[iter_map->first].second, on_off_m[iter_map->first]);

		// For the VMon graph
		set_HV_axis(iter_map->second.first, "VMon (V)"); // Will set axis and write VMon TGraph to rootfile. iter_map->second is the value in the map and doing .first gives first element of the pair of TGraphs.
		set_HV_axis(graphs_HV_extrapol_map[iter_map->first].first, "VMon extr(V)");

		// For the IMon graph
		set_HV_axis(iter_map->second.second, "IMon (#muA)");
		set_HV_axis(graphs_HV_extrapol_map[iter_map->first].second, "IMon(#muA)");

		if (graphs_HV_extrapol_map[iter_map->first].first->GetN() && graphs_HV_extrapol_map[iter_map->first].second->GetN())
		{
			// Keep memory of the graphs needing the extrapolation (for the output_commands_to_root)
			HV_graphs_extrapol.push_back( make_pair(graphs_HV_extrapol_map[iter_map->first].first->GetName(), graphs_HV_extrapol_map[iter_map->first].second->GetName()) );

			
		}
	}

	system("if [ -e list_of_files_HV ]\nthen rm list_of_files_HV\nfi");
	system("if [ -e data_files ]\nthen rm data_files\nfi");

	delete rootfile;


	// If HV_graphs_draw is empty and user wanted graphs drawn, it is because our code did not encouter a dataset within time range required by user.
	if (!HV_graphs.size() && draw_or_not)
	{
		cout << "\n** Info **: No graph was saved to the rootfile (Tip: Are you certain your time range contained data?)\n";
	}
	// If there ARE graphs to draw AND the user wants them drawn
	else 
	{
		cout_oldest_newest_files(folder_names_vector);
		cout << "\nChanges added to \"" << rootfile_name << "\" file." << endl;
		if (draw_or_not)
		{
			string selection_in_list;	// The number of the selection from the list 
			if (automatic)
			{
				selection_in_list='0';	// This will choose the option "ALL" in the list(even though it will not be explicitely displayed)
				goto inside_while_loop2;	// To enter the while loop by skipping the getline
			}
			if (HV_graphs.size())	// We only show the graph list if we are in manual mode (and if there ARE graphs to be drawn)
			{
				// Order all the sensors in sensor_list in alphabetical order
				sort(HV_graphs.begin(), HV_graphs.end());
				cout << "\n\tList of available HV plots to draw:\n\t..................................................\n";
				for (size_t i=0; i < HV_graphs.size(); ++i)	// Will create a list of the available HV graphs to plot
				{
					cout << "\t" << i+1 << ")\t" << HV_graphs.at(i).first << "_imon" << endl;	//Added 1.
				}
				cout << "\t0)\tALL\n";
				cout << "\t..................................................\nChoose HV graphs you want to draw from the list.\nSelection #: ";
				// This while loop will accumulate the selected graphs to draw.
				while (getline(cin, selection_in_list) && selection_in_list.compare("done"))	// If we select "done" or '0' (all sensors), the while loop is over.
				{
					inside_while_loop2:	// This is for the automatic case, enables skipping the getline in the while argument
					if (selection_in_list.length())	//Make sure the user inputed something (prevent pressing enter by mistake)
					{
						if (selection_in_list =='0')
						{
							cout << "Selected all plots.\n";
							break;
						}
						else
						{
							size_t select_num=atoi(selection_in_list.c_str());
							if (0 < atoi(selection_in_list.c_str()) && select_num <= HV_graphs.size())	// If the number of the selection is in the range of the available options in the menu
							{
								if (new_vector_item(HV_graphs_draw, HV_graphs.at(atoi(selection_in_list.c_str())-1)))	// Insert selection graph into the 'to draw' list. Substract 1, because in the list we added 1 (search 'Added 1.')
								{
									cout << "Added " << HV_graphs.at(select_num-1).first << "_imon to selection.\n";
								}
								else
								{
									cout << "You already selected # " << selection_in_list << ".\n";
								}
							}
							else
							{
								cout << "Option \"" << selection_in_list << "\" is not available.\n";
							}
							cout << "Selection # (press \"enter\" to end): "; 
						}
					}
					else	// If selection is empty
					{
						if (HV_graphs_draw.size())	// If user has selected at least one graph to plot
						{
							break;	// Can escape the while loop
						}
						cout << "Error: You must select a sensor (or type \"done\" if you are really done).\nSelection #: ";
					}
				}
				if (selection_in_list=='0')	// If user selected 0
				{
					HV_graphs_draw = HV_graphs;
				}
			}
		}
	}
}
