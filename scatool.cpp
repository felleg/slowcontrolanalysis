//To use this code, write in the shell the following command:
//	./runAnalysis.sh (for manual mode, contains full menu. Probably the one you want!)
//	./runAnalysisAuto.sh (for automatic mode, used by cronjob for website monitoring)

// READ the README, it is constantly updated at each release.

// Website for monitoring is located at http://www.hep.physics.mcgill.ca/~legerf/SCATool/

// Written by F�lix L�ger, July 2015 - Present (still in development)
// contact at: legerf@physics.mcgill.ca

#include "functions.h"
using namespace std;

int main(int argc, char* argv[])
{
	vector<string> sensor_graphs;	// All the sensor graphs selected
	vector<string> sensor_graphs_draw;	// Only the sensors the user wants in the ROOT macro (drawn)
	vector<pair<string, string> > HV_graphs;	//HV_graphs selected (not necessarily drawn, but in rootfile)
	vector<pair<string, string> > HV_graphs_draw;	// HV graphs that will be drawn by the ROOT macro
	vector<pair<string, string> > HV_graphs_extrapol;	// HV graphs that require we add an extrapolation
	vector<string> cor_graphs;	// Correlation graphs selected (not necessarily drawn)
	vector<string> cor_graphs_draw;	// All correlation graphs to be in the ROOT macro (drawn)

	// Checks the runAnalysis.sh or runAnalysisAuto.sh arguments
	if (argc>1)
	{
		// Checks if we use automatic or manual mode
		string line=argv[1];
		if (line == "-a") 
		{
			automatic = 1;

			// If we use automatic, checks if we produce only 24h graphs or week and month
			if (argc>2)
			{
				string line=argv[2];
				if (line == "-d") 
				{
					cout << "\nINFO: OPTION -d SELECTED. THIS WILL DRAW ONLY 24H GRAPHS.\n\n";
					only_24h = 1;
				}
			}
			else 
			{
				cout << "\nTip: To save on execution time, you can use option -d when starting runSCAToolAuto.sh to draw only the graphs for the last 24h (omits last week and month).\n\n";
				only_24h  = 0;
			}
		}
		else automatic = 0;

	}

	if (automatic)
	{
		string line;
		if (argc>2) line=argv[2];
		if (line != "-hv") 
		{
			data_to_graph(sensor_graphs, sensor_graphs_draw);	// Draw all the sensor graphs (selecting the -hv option as an argument when calling the automatic mode will NOT draw the sensor graphs)
		}
		if (line != "-s") 
		{
			analyze_HV(HV_graphs, HV_graphs_draw, HV_graphs_extrapol);		// Draw all the HV graphs for last month (selecting the -s option as an argument when calling the automatic mode will NOT draw the HV graphs)
		}
		//correlation_graph(sensor_graphs, cor_graphs_draw);		// Draw all the correlations (CURRENTLY NOT FOR AUTOMATIC MONITORING. WILL UNCOMMENT WHEN READY TO IMPLEMENT.)
		output_commands_to_root(sensor_graphs, sensor_graphs_draw, cor_graphs, cor_graphs_draw, HV_graphs, HV_graphs_draw, HV_graphs_extrapol, only_24h);	// Make ROOT macro
	}

	else
	{
		list_menu(sensor_graphs_draw.size(), cor_graphs_draw.size(), HV_graphs_draw.size());
		char menu='.';
		string line;
		while (menu != 'q' && getline(cin,line))
		{
			menu = line[0];	// Take only the first character of the input, make sure we don't get '\n' character
			switch(menu)
			{
				case '1':
					cout << "||_____ a) Gas System Sensors Graphs\n|______ b) HV Graphs\n\n\tSelection: ";
					while (menu != 'a' && menu != 'b' && getline(cin,line))
					{
						menu = line[0];	
						switch(menu)
						{
							case 'a': 
								cout << "====================\n";
								data_to_graph(sensor_graphs, sensor_graphs_draw);
								end_execution(sensor_graphs_draw.size(), cor_graphs_draw.size(), HV_graphs_draw.size());
								break;
							case 'b':
								cout << "====================\n";
								/* FUNCTIONS FOR THE HV */
								analyze_HV(HV_graphs, HV_graphs_draw, HV_graphs_extrapol);
								end_execution(sensor_graphs_draw.size(), cor_graphs_draw.size(), HV_graphs_draw.size());
								break;
							default:
								cout << "\tChoice \"" << menu << "\" not available. Try again: ";
								break;
						}
					}
					break;

				case '2':
					cout << "============\n";
					if (rootfile_contains(rootfile_name, "Sensor_"))	// Only enable correlation graphs if a rootfile containing graphs named "Sensor_<name>" exists in directory
					{
						correlation_graph(sensor_graphs, cor_graphs, cor_graphs_draw);
						end_execution(sensor_graphs.size(), cor_graphs_draw.size(), HV_graphs_draw.size());
					}
					else
					{
						cout << "Choice \"" << menu << "\" not available. Try again: ";
					}
					break;

				case 'q':
					cout << "============\n\n";
					output_commands_to_root(sensor_graphs, sensor_graphs_draw, cor_graphs, cor_graphs_draw, HV_graphs, HV_graphs_draw, HV_graphs_extrapol, only_24h);
					break;

				default:
					cout << "Choice \"" << menu << "\" not available. Try again: ";
					break;
			}
		}
	}
	return sensor_graphs_draw.size() + HV_graphs_draw.size() + cor_graphs_draw.size();
}
